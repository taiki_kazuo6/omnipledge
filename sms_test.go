package omnipledge_test

import (
	"appengine/aetest"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/r1cky1337/omnipledge-subscription"
)

var _ = Describe("Sms", func() {

	Context("Testing SMS", func() {

		XIt("Should send text message from OP #", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			SendTextMessage(context, "+13046103672", "hello world")
			Ω(true).Should(BeTrue())
		})
	})

})
