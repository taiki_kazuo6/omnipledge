package omnipledge

import (
	"appengine"
	"appengine/memcache"
	"fmt"
	"github.com/dchest/uniuri"
	"time"
)

//untested
func ShortURLCode(c appengine.Context, url string) (string, error) {
	twoDaysAhead := time.Now().AddDate(0, 0, 2)
	duration := twoDaysAhead.Sub(time.Now())
	key := uniuri.NewLen(5)
	item := &memcache.Item{
		Key:        key,
		Value:      []byte(url),
		Expiration: duration,
	}
	if err := memcache.Set(c, item); err != nil {
		return "", fmt.Errorf("Error adding item: %v to memcache", err)
	}

	return key, nil //fmt.Sprintf("%s/d/%s", key), nil
}

//untested
func GetURLForShortCode(c appengine.Context, code string) (string, error) {
	item, e := memcache.Get(c, code)
	if e != nil {
		return "", e
	}
	return string(item.Value), nil
}
