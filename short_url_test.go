package omnipledge_test

import (
	"appengine/aetest"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/r1cky1337/omnipledge-subscription"
)

var _ = Describe("ShortUrl", func() {
	Context("Testing Short URL", func() {
		It("Should test generating a short url and retrieving it again", func() {
			c, _ := aetest.NewContext(nil)
			defer c.Close()
			url := "http://www.google.com/w/t/f"
			code, sEr := ShortURLCode(c, url)
			Ω(sEr).ShouldNot(HaveOccurred())
			gotURL, getEr := GetURLForShortCode(c, code)
			Ω(getEr).ShouldNot(HaveOccurred())
			Expect(url).To(Equal(gotURL))
		})
	})
})
