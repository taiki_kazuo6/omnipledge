package omnipledge

import (
	"appengine"
	"appengine/datastore"
	"encoding/csv"
	"fmt"
	"github.com/martini-contrib/render"
	"io"
	"net/http"
	"strings"
	"time"
)

type Contact struct {
	ID        string `datastore:"-"`
	Timestamp int64
	OrgID     string
	Name      string
	Phone     string
}

func HandleContactsUpload(res http.ResponseWriter, r render.Render, req *http.Request) {

	c := appengine.NewContext(req)

	// Get the account to store the contacts in
	qs := req.URL.Query()
	email := qs.Get("email")
	org := &Organization{}
	if err := org.GetByEmail(c, email); err != nil {
		LogError(c, "Error getting oganization by Email", err)
		ReturnErrorUnauthorized(r)
		return
	}

	// Get the data, build the CSV, and check for errors

	contactFile, hdr, err := req.FormFile("file")
	if err != nil {
		if err == http.ErrMissingFile || err == http.ErrNotMultipart {
			ReturnErrorValidation(r, "Invalid CSV file")
			return
		} else {
			LogError(c, "Contact upload error", err)
			ReturnErrorValidation(r, "")
			return
		}
	}
	defer contactFile.Close()

	contentType := hdr.Header["Content-Type"][0]
	c.Debugf("Content-type: %s", contentType)
	if !isAllowedContentType(contentType) {
		ReturnErrorValidation(r, "Invalid CSV file")
		return
	}

	//TODO: This should be done in a task
	// Get a map of phone numbers to names from the csv
	importEr := SyncImportedAndExistingContacts(contactFile, org, c)
	if importEr != nil {
		ReturnErrorValidation(r, "Problem importing from CSV")
		return
	}

}

func SyncImportedAndExistingContacts(contactFile io.Reader, org *Organization, c appengine.Context) error {
	numberNameMap, err := NumberNameMapFromCSV(contactFile)
	if err != nil {
		c.Errorf("Error parsing CSV: %v", err)
		return err
	}

	// Get all existing contacts
	existingContacts, getEr := org.GetAllContacts(c)
	if getEr != nil {
		c.Errorf("Error getting existing contats: %v", getEr)
		return getEr
	}

	// Create two maps that we will use to decide if the contact does not exist, or eixsts and needs modified:
	// PhoneNumber:Name
	// PhoneNumber:DatastoreKey

	exNameMap, exKeyMap := MapSavedContactNameAndID(existingContacts)

	// We put all contacts to be added and modified in this slice
	newAndModifiedContacts := []*Contact{}

	// Iterate through all CSV imported contacts
	for num, currentName := range numberNameMap {
		// Check to see if they are present in the existing contact list
		existingNameValue, prs := exNameMap[num]
		// If they are not present, or the imported name value is different, add it to new/modified slice
		if !prs || existingNameValue != currentName {
			contactToSave := &Contact{
				Name:      currentName,
				Phone:     num,
				OrgID:     org.ID,
				Timestamp: time.Now().Unix(),
			}
			// If it is already present, use the existing datastore key
			if prs {
				contactToSave.ID = exKeyMap[num]
			}
			newAndModifiedContacts = append(newAndModifiedContacts, contactToSave)
		}
	}

	for _, contact := range newAndModifiedContacts {
		er := contact.Save(c)
		if er != nil {
			c.Errorf("Error saving a new/mod'd contact from CSV import", er)
		}
	}

	return nil
}

func MapSavedContactNameAndID(contactList []*Contact) (map[string]string, map[string]string) {
	existingNumberNameMap := make(map[string]string)
	existingNumberDSKeyMap := make(map[string]string)

	for _, eCont := range contactList {
		existingNumberNameMap[eCont.Phone] = eCont.Name
		existingNumberDSKeyMap[eCont.Phone] = eCont.ID
	}

	return existingNumberNameMap, existingNumberDSKeyMap
}

func NumberNameMapFromCSV(file io.Reader) (map[string]string, error) {
	lines, err := ParseCSV(file)
	if err != nil {
		return nil, err
	}

	// Check for problems reading the CSV
	// Check for at least 2 column headings: name and number

	colHeadings := lines[0]
	if len(colHeadings) < 2 {
		return nil, fmt.Errorf("Error reading all lines")
	}
	data := lines[1:]
	names := NameColumn(colHeadings, data)
	numbers := PhoneColumn(colHeadings, data)

	nameNumbers, er := NumberNameMap(names, numbers)
	if er != nil {
		return nameNumbers, er
	}
	return nameNumbers, nil
}

func NumberNameMap(names, numbers []string) (map[string]string, error) {
	if len(names) != len(numbers) {
		return nil, fmt.Errorf("Name and Number slice are different lengths")
	}
	toReturn := make(map[string]string)
	for i, n := range numbers {
		if n != "" {
			toReturn[n] = names[i]
		}
	}

	return toReturn, nil
}

func E164Format(number string) string {
	ok := "+0123456789"
	toReturn := ""
	for _, d := range number {
		if strings.ContainsRune(ok, d) {
			toReturn += string(d)
		}
	}

	if len(toReturn) == 10 {
		toReturn = "+1" + toReturn
	}

	return toReturn
}

func ParseCSV(file io.Reader) ([][]string, error) {
	reader := csv.NewReader(file)
	return reader.ReadAll()
}

func PhoneColumn(colHeaders []string, data [][]string) []string {
	toReturn := []string{}

	phoneIx := PhoneColumnIndex(colHeaders)
	for _, line := range data {
		toAdd := strings.TrimSpace(line[phoneIx])
		toAdd = E164Format(toAdd)
		toReturn = append(toReturn, toAdd)
	}

	return toReturn
}

func NameColumn(colHeaders []string, data [][]string) []string {

	toReturn := []string{}

	//Are there first and last name columns
	if FirstNameColumnIndex(colHeaders) < 0 || LastNameColumnIndex(colHeaders) < 0 {
		nameIx := SingleNameColumnIndex(colHeaders)
		for _, line := range data {
			toAdd := fmt.Sprintf("%s", strings.TrimSpace(line[nameIx]))
			toReturn = append(toReturn, toAdd)
		}
	} else {
		fNameColIx := FirstNameColumnIndex(colHeaders)
		lNameColIx := LastNameColumnIndex(colHeaders)
		for _, line := range data {
			toAdd := fmt.Sprintf("%s %s", strings.TrimSpace(line[fNameColIx]), strings.TrimSpace(line[lNameColIx]))
			toReturn = append(toReturn, toAdd)
		}
	}

	return toReturn
}

func SingleNameColumnIndex(colHeaders []string) int {
	sinNameCat := []string{"name"}
	return FirstInstanceOfTermInArray(sinNameCat, colHeaders)
}

func FirstNameColumnIndex(colHeaders []string) int {
	firstNameCats := []string{"first name", "first"}
	return FirstInstanceOfTermInArray(firstNameCats, colHeaders)
}

func LastNameColumnIndex(colHeaders []string) int {
	lastNameCats := []string{"last name", "last"}
	return FirstInstanceOfTermInArray(lastNameCats, colHeaders)
}

func PhoneColumnIndex(colHeaders []string) int {
	numberCategories := []string{"cell", "mobile", "phone", "number", "phone number", "MOBILE PHONE NUMBER"}
	return FirstInstanceOfTermInArray(numberCategories, colHeaders)
}

func FirstInstanceOfTermInArray(terms, array []string) int {
	for i, element := range array {
		element = strings.ToLower(element)
		element = strings.TrimSpace(element)
		for _, term := range terms {
			term = strings.ToLower(term)
			if term == element {
				return i
			}
		}
	}
	return -1
}

var allowedContentTypes = []string{
	"text/plain",
	"text/csv",
}

func isAllowedContentType(contentType string) bool {
	fmt.Println("CHECKING", contentType)
	for _, value := range allowedContentTypes {
		if contentType == value {
			return true
		}
	}

	return false
}

// Contact Object Persistence Methods

// Contact Delete
// Build delete http call

func (this *Contact) Delete(c appengine.Context) error {
	dsKey, erDecoding := datastore.DecodeKey(this.ID)
	if erDecoding != nil {
		return erDecoding
	}
	return datastore.Delete(c, dsKey)
}

func (this *Contact) GetByPhone(c appengine.Context, orgID, phoneNumber string) error {

	org := &Organization{}
	if err := org.GetByID(c, orgID); err != nil {
		return fmt.Errorf("Error finding parent organization by id: %s", err)
	}
	orgKey, erDecoding := datastore.DecodeKey(org.ID)
	if erDecoding != nil {
		return erDecoding
	}

	//get org ancestor key

	var contacts []Contact
	q := datastore.NewQuery("Contact").Filter("Phone =", phoneNumber).Ancestor(orgKey)
	keys, err := q.GetAll(c, &contacts)
	if err != nil {
		return err
	}
	//c.Infof("keys: %d", len(keys))
	if len(keys) > 0 {
		contactKey := keys[0]
		if e := datastore.Get(c, contactKey, this); e != nil {
			return e
		}
		this.ID = contactKey.Encode()
		c.Infof("Returning Contact: %+v", this)
	}
	return nil
}

func (this *Contact) GetByID(c appengine.Context, ID string) error {
	conKey, e := datastore.DecodeKey(ID)
	if e != nil {
		return e
	}
	err := datastore.Get(c, conKey, this)
	this.ID = ID
	if err != nil {
		return err
	}
	return nil
}

func (this *Contact) Save(c appengine.Context) error {
	org := &Organization{}
	//VALIDATION
	//Requires organization to be saved
	if err := org.GetByID(c, this.OrgID); err != nil {
		return fmt.Errorf("Error finding parent organization by id: %s", err)
	}

	orgKey, erDecoding := datastore.DecodeKey(org.ID)
	if erDecoding != nil {
		return erDecoding
	}
	this.Timestamp = time.Now().Unix()
	var myKey *datastore.Key
	if this.ID == "" {
		myKey = datastore.NewKey(c, "Contact", "", 0, orgKey)
	} else {
		var err error
		myKey, err = datastore.DecodeKey(this.ID)
		if err != nil {

		}
	}
	savekey, e := datastore.Put(c, myKey, this)
	if e != nil {
		return e
	}
	this.ID = savekey.Encode()
	return nil
}
