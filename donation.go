package omnipledge

import (
	"appengine"
	"appengine/datastore"
	"fmt"
	"time"
)

type Donation struct {
	ID             string `datastore:"-"`
	DonorID        string
	Amount         int64 //In cents
	StripeChargeID string
	Timestamp      int64
	Created        int64
	Completed      bool
	PaidOut        bool
	OrgID          string
	Method         string //either sms or web
	//These are redundant to the donor but necessary to persist the donation before the donor is known
	Name          string
	Email         string
	Phone         string
	PhoneVerified bool
}

const (
	OPDonationMethodWeb = "web"
	OPDonationMethodSMS = "sms"
)

func DonationsOlderThan(c appengine.Context, timestamp int64) ([]*datastore.Key, error) {
	var donations []Donation
	q := datastore.NewQuery("Donation").Filter("Timestamp <=", timestamp).Filter("Completed = ", false).Ancestor(orgListKey(c))
	toReturn, err := q.GetAll(c, &donations)
	if err != nil {
		return nil, err
	}
	return toReturn, nil
}

func PruneOldDonations(c appengine.Context) {
	twoWeeks := int64(60 * 60 * 24 * 7 * 2)
	now := time.Now().Unix()
	twoWeeksAgo := now - twoWeeks
	toDelete, err := DonationsOlderThan(c, twoWeeksAgo)
	if err != nil {
		LogError(c, "Problem getting donations to prune", err)
	}

	if len(toDelete) > 0 {
		deleteEr := datastore.DeleteMulti(c, toDelete)
		if deleteEr != nil {
			LogError(c, "Problem deleting old donatinons", deleteEr)
		} else {
			LogDebug(c, fmt.Sprintf("Deleted %v old donations", len(toDelete)))
		}
	}
}

func (this *Donation) UpdateWithRequest(c appengine.Context, data DonationRequest) error {
	//VALIDATION
	// if data.Name == "" || data.Email == "" {
	// 	return fmt.Errorf("Name and email address is required")
	// }
	this.Name = data.Name
	this.Email = data.Email
	this.Phone = data.Phone
	this.Amount = data.Amount
	return nil
}

func (this *Donation) GetByCharge(c appengine.Context, orgID, chargeID string) error {

	org := &Organization{}
	if err := org.GetByID(c, orgID); err != nil {
		return fmt.Errorf("Error finding parent organization by id: %s", err)
	}
	orgKey, erDecoding := datastore.DecodeKey(org.ID)
	if erDecoding != nil {
		return erDecoding
	}

	//get org ancestor key

	var donations []Donation
	q := datastore.NewQuery("Donation").Filter("StripeChargeID =", chargeID).Ancestor(orgKey)
	keys, err := q.GetAll(c, &donations)
	if err != nil {
		return err
	}
	//c.Infof("keys: %d", len(keys))
	if len(keys) > 0 {
		donationKey := keys[0]
		if e := datastore.Get(c, donationKey, this); e != nil {
			return e
		}
		this.ID = donationKey.Encode()
		c.Infof("Returning Donor: %+v", this)
	}
	return nil
}

func (this *Donation) GetByID(c appengine.Context, ID string) error {
	donKey, e := datastore.DecodeKey(ID)
	if e != nil {
		return e
	}
	err := datastore.Get(c, donKey, this)
	this.ID = ID
	if err != nil {
		return err
	}
	return nil
}

func (this *Donation) Save(c appengine.Context) error {
	org := &Organization{}
	//VALIDATION
	//Requires organization to be saved
	if err := org.GetByID(c, this.OrgID); err != nil {
		return fmt.Errorf("Error finding parent organization by id: %s", err)
	}

	if this.Amount < 0 {
		return fmt.Errorf("Donation cannot be negative")
	}

	orgKey, erDecoding := datastore.DecodeKey(org.ID)
	if erDecoding != nil {
		return erDecoding
	}
	this.Timestamp = time.Now().Unix()
	var myKey *datastore.Key
	if this.ID == "" {
		myKey = datastore.NewKey(c, "Donation", "", 0, orgKey)
	} else {
		var err error
		myKey, err = datastore.DecodeKey(this.ID)
		if err != nil {

		}
	}
	savekey, e := datastore.Put(c, myKey, this)
	if e != nil {
		return e
	}
	this.ID = savekey.Encode()
	return nil
}
