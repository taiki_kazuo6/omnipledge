package omnipledge_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestOmnipledge(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Omnipledge Suite")
}
