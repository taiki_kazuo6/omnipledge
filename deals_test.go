package omnipledge_test

import (
	"appengine/aetest"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/r1cky1337/omnipledge-subscription"
)

var _ = Describe("Deals", func() {
	Context("Testing Deals", func() {

		It("Should save a new deal", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			newDeal := &Deal{}
			newDeal.OrganizationID = "test"
			newDeal.DonationPercent = 0.03
			newDeal.DonationFee = 30
			newDeal.Affiliate = true
			err := newDeal.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(newDeal.ID).ToNot(Equal(""))
		})

		It("Should get deal by ID", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			// Save a deal
			newDeal := &Deal{}
			newDeal.OrganizationID = "test"
			newDeal.DonationPercent = 0.03
			newDeal.DonationFee = 30
			newDeal.Affiliate = true
			_ = newDeal.Save(context)
			dID := newDeal.ID

			//Retrieve it by ID

			oldDeal := &Deal{}
			getEr := oldDeal.GetByID(context, dID)
			Ω(getEr).ShouldNot(HaveOccurred())
			Expect(oldDeal.ID).To(Equal(dID))
		})

		It("Should get deal by Organization ID", func() {
			context, _ := aetest.NewContext(nil)
			newDeal := &Deal{}
			newDeal.OrganizationID = "test"
			newDeal.DonationPercent = 0.03
			newDeal.DonationFee = 30
			newDeal.Affiliate = true
			_ = newDeal.Save(context)

			oldDeal := &Deal{}
			getEr := oldDeal.GetByOrgID(context, "test")
			Ω(getEr).ShouldNot(HaveOccurred())
			Expect(oldDeal.ID).To(Equal(newDeal.ID))
		})

	})
})
