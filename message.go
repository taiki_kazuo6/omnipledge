package omnipledge

import (
	"appengine"
	"appengine/datastore"
	"fmt"
	"strings"
	"time"
)

type Message struct {
	ID            string `datastore:"-"`
	Timestamp     int64
	OrgID         string
	Body          string
	Live          bool
	Recipients    []string //Contact or Donor IDs
	ResponseCount int
}

//Necessary HTTP Methods
//HTTP PUT: Edit message draft
//HTTP DELETE: Delete message draft
//HTTP GET: Return HTML template for drafting a new message populated with the orgs' text code callback

//TODO: Increment message response count in SMS responder machine
func (this *Message) Send(c appengine.Context) error {
	if this.ID == "" {
		return fmt.Errorf("Cannot send. Unsaved message")
	} else if this.Live {
		return fmt.Errorf("Cannot send. This message has already been sent.")
	} else if len(this.Recipients) == 0 {
		return fmt.Errorf("Cannot send. No recipients.")
	} else if this.Body == "" {
		return fmt.Errorf("Cannot send. No body.")
	}
	for _, recipID := range this.Recipients {
		_, number := InfoForContactOrDonor(recipID, c)
		if number != "" {
			SendTextMessage(c, number, this.Body)
			c.Debugf("sent:%s to %s", this.Body, number)
		}
	}
	this.Live = true
	saveEr := this.Save(c)
	if saveEr != nil {
		c.Debugf("Could not save message with ID: %s", this.ID)
		LogError(c, "Problem saving sent message – not marked as live", saveEr)
		return saveEr
	}

	return nil
}

func (this *Organization) TextCodeCallbackString() string {
	return fmt.Sprintf("Respond with %s donate.", strings.ToUpper(this.TextCode))
}

//Returns the name and number
func InfoForContactOrDonor(contactID string, c appengine.Context) (string, string) {
	// There will likely be less donors, so we'll check there for the ID first
	donor := &Donor{}
	getEr := donor.GetByID(c, contactID)
	if getEr != nil || donor.ID != "" {
		return donor.Name, donor.Phone
	}

	contact := &Contact{}
	getEr = contact.GetByID(c, contactID)
	if getEr != nil || contact.ID != "" {
		return contact.Name, contact.Phone
	}

	c.Debugf("POTENTIAL PROBLEM: Message contact ID does not return contact or donor")
	return "", ""
}

func (this *Message) GetByID(c appengine.Context, ID string) error {
	c.Debugf("ID len is: %i", len(ID))
	c.Debugf("Trimmed len is: %i", len(strings.TrimSpace(ID)))

	mesKey, e := datastore.DecodeKey(ID)
	if e != nil {
		c.Debugf("Error decoding ID")
		return e
	}
	err := datastore.Get(c, mesKey, this)
	this.ID = ID
	if err != nil {
		c.Debugf("Error getting message")
		return err
	}
	return nil
}

func (this *Message) Save(c appengine.Context) error {
	org := &Organization{}
	//VALIDATION
	//Requires organization to be saved
	if err := org.GetByID(c, this.OrgID); err != nil {
		return fmt.Errorf("Error finding parent organization by id: %s", err)
	}

	orgKey, erDecoding := datastore.DecodeKey(org.ID)
	if erDecoding != nil {
		return erDecoding
	}
	this.Timestamp = time.Now().Unix()
	var myKey *datastore.Key
	if this.ID == "" {
		myKey = datastore.NewKey(c, "Message", "", 0, orgKey)
	} else {
		var err error
		myKey, err = datastore.DecodeKey(this.ID)
		if err != nil {

		}
	}
	savekey, e := datastore.Put(c, myKey, this)
	if e != nil {
		return e
	}
	this.ID = savekey.Encode()
	return nil
}
