package omnipledge

import (
	"crypto/hmac"
	"crypto/sha256"
	"crypto/tls"
	"encoding/hex"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-martini/martini"
	"github.com/martini-contrib/binding"
	"github.com/martini-contrib/render"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/client"

	"appengine"
	"appengine/mail"
	// "appengine/urlfetch"
)

const (
	//Local
	OPBaseURL = "http://localhost:8080/"
	//Staging
	// OPBaseURL = "https://omnipledge-staging.appspot.com/"
	//Productiion
	//OPBaseURL = "https://www.omnipledge.com/"

	OPWHLiveMode = false

	//Staging
	OPEmailAddress = "accounts@omnipledge-staging.appspotmail.com"
	//Production
	//OPEmailAddress = "accounts@omnipledge.com"

	//Testing
	OPStripeAPI = "sk_test_h5tjiswt5ItcISJ8m9gqVUOM"

	// If they go over the limit $300 monthly limit:
	// Charge 5% + $0.39
	OPSubscriptionPeriodLimit            = 30000
	OPSubscriptionDonationPercentage     = 0.05
	OPSubscriptionDonationTransactionFee = 39

	//Staging
	OPTwilioNumber = "+18556664788"
	OPTwilioSID    = "AC7255c7e74557bc0d7f2244e2ed867744"
	OPTwilioAuth   = "c7f5c266903bc3d0452bb9eb49853ea1"

	OPServerKey                  = "FPB8kUyyCnzZW7ktt"
	OPOrganizationStripeMetaKey  = "OrganizationStripeAccount"
	OPOrganizationPaidOutMetaKey = "PaidOut"
	OPOrganizationIDMetaKey      = "OrgIdentifier"

	OPCampaignrTransferKey = "campaignr"
)

func init() {
	// SERVER ROUTING
	m := martini.Classic()
	m.Use(render.Renderer(render.Options{
		Directory:  "public", // Specify what path to load the templates from.
		Extensions: []string{".html"},
		Delims:     render.Delims{"{[{", "}]}"},
	}))
	m.Use(AddHeaders)
	m.Use(martini.Static("public"))

	//Signup
	m.Post("/api/v2/account", ParseSignup(SignupRequest{}), HandleSignup)
	m.Post("/api/v2/account/reset", ParseResetPasswordEmail(ResetRequest{}), HandleSendResetMessage)
	m.Get("/api/v2/account/:email/:token/confirm", HandleConfirmation)
	m.Post("/api/v2/account/change-password/:email", ParseChangePasswordRequest(ChangeRequest{}), HandleChangePassword)
	m.Post("/api/v2/account/login", ParseLogin(LoginRequest{}), HandleLogin)
	m.Post("/api/v2/account/bank", Auth, ParseBankAccount(BankAccountRequest{}), HandleBankInfo)
	m.Get("/api/v2/account/auth/test", Auth, func() string {
		return "Hello Authenticated world!"
	})

	m.Get("/api/v2/donor", Auth, HandleGetDonors)
	m.Get("/api/v2/donation", Auth, HandleGetDonations)
	m.Get("/api/v2/donation/:donationID", HandleGetDonation)
	m.Post("/api/v2/donation/:orgID/:donationID", ParseDonation(DonationRequest{}), HandleWebDonation)

	//m.Put("/api/v1/donation/:donationID", ParseDonation(DonationRequest{}), HandleDonationUpdate)
	//Twilio
	m.Get("/api/v1/twilio", HandleTwilioWebHook)

	//For Admin use - Creating deals that can be customized in datastore
	// m.Get("/api/v1/deals/create", HandleCreateDeal)

	// For uploading contacts to send massive SMS messages to
	m.Post("/api/v1/contact/upload", Auth, HandleContactsUpload)

	m.Get("/api/v1/contact", Auth, HandleGetContacts)
	m.Get("/api/v1/contact/:contactID", Auth, HandleGetContact)
	m.Delete("/api/v1/contact/:contactID", Auth, HandleDeleteContact)

	// Save a mass message draft
	m.Get("/api/v1/message/send/:mID", Auth, HandleSendMessage)
	m.Post("/api/v1/message", Auth, ParseMessageRequest(MessageRequest{}), HandlePostMessage)
	m.Put("/api/v1/message/:mID", Auth, ParseMessageRequest(MessageRequest{}), HandlePutMessage)
	m.Get("/api/v1/message", Auth, HandleGetMessage)

	// Subscription payment route
	m.Post("/api/v2/subscription/set-payment-info", Auth, ParseSubscriptionRequest(SubscriptionPaymentSetRequest{}), HandleSetSubscriptionPayment)

	// Cancel subscription route
	m.Post("/api/v2/subscription/cancel", Auth, HandleCancelSubscription)

	// Update donor card route (also check in mobile payment if card is expired)
	m.Post("/api/v2/donation/donorupdate", ParseDonorPaymentUpdateRequest(DonorPaymentUpdateRequest{}), HandleDonorPaymentUpdateRequest)

	// Donor card needs updated : charge.failed
	m.Post("/wh/account/charge/failed", ParseStripeEvent(stripe.Event{}), HandleFailedDonationWebhook)
	m.Post("/wh/account/charge/success", ParseStripeEvent(stripe.Event{}), HandleSuccessfulDonationWebhook)

	// Organization payment failed: card needs updated invoice.failed / succeeded
	m.Post("/wh/account/invoice/failed", ParseStripeEvent(stripe.Event{}), HandleFailedInvoiceWebhook)
	m.Post("/wh/account/invoice/success", ParseStripeEvent(stripe.Event{}), HandleSuccessfulInvoiceWebhook)

	// Organization subscription cancelled : customer.subscription.deleted
	m.Post("/wh/account/subscription/cancelled", ParseStripeEvent(stripe.Event{}), HandleCancelledSubscriptionWebhook)

	// Organization subscription renewed : customer.subscription.created
	m.Post("/wh/account/subscription/renewed", ParseStripeEvent(stripe.Event{}), HandleRenewedSubscriptionWebhook)

	// Organization needs more verification info : account.updated
	m.Post("/wh/account/updated", ParseStripeEvent(stripe.Event{}), HandleAccountUpdated)

	//For CORS
	m.Options("/**", func() (int, string) {
		return 200, ""
	})

	//Static pages
	m.Get("/donate/:orgTextCode", HandleDonationPageRedirect)
	m.Get("/donation", HandleGetPageDonation)
	m.Get("/donorupdate", HandleGetPageDonorUpdate)
	m.Get("/d/:dID", HandleShortURL)

	// m.Get("/u/:donorID", HandleDonorInfoUpdate)

	http.Handle("/", m)
}

// Webhook handlers
func HandleAccountUpdated(data stripe.Event, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	stripeID := ""

	if data.Live != OPWHLiveMode {
		return
	}

	if str, ok := data.Data.Obj["id"].(string); ok {
		stripeID = str
	}

	log := fmt.Sprintf("Stripe ID: %s", stripeID)
	LogDebug(con, log)

	if e := org.GetByManagedAccountId(con, stripeID); e != nil {
		LogDebug(con, "Error getting customer by stripe account ID")
	}

	if org.ID == "" {
		return
	}

	var verif map[string]interface{}
	var fieldsNeeded []string
	var dueBy int64
	if v, ok := data.Data.Obj["verification"].(map[string]interface{}); !ok {
		LogDebug(con, "Problem parsing verification information")
	} else {
		verif = v
	}

	if f, ok := verif["fields_needed"].([]string); !ok {
		LogDebug(con, "Problem parsing fields needed information")
	} else {
		fieldsNeeded = f
	}

	if d, ok := verif["due_by"].(int64); !ok {
		LogDebug(con, "Problem parsing due by information")
	} else {
		dueBy = d
	}

	org.VerificationFieldsNeeded = fieldsNeeded
	org.VerificationDueBy = dueBy
	if se := org.Save(con); se != nil {
		LogDebug(con, "Error saving org.")
	}
}
func HandleAcctVerificationWebhook(data stripe.Event, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	stripeID := ""

	if data.Live != OPWHLiveMode {
		return
	}

	if str, ok := data.Data.Obj["customer"].(string); ok {
		stripeID = str
	}

	log := fmt.Sprintf("Stripe ID: %s", stripeID)
	LogDebug(con, log)

	if e := org.GetBySubscriptionCustomerId(con, stripeID); e != nil {
		fmt.Println("Error getting customer by stripe ID")
	}

	if org.ID == "" {
		return
	}

	org.PaymentInformationNeedsUpdated = true
	org.Deactivated = true
	if se := org.Save(con); se != nil {
		fmt.Println("Error saving org.")
	}
}
func HandleRenewedSubscriptionWebhook(data stripe.Event, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	stripeID := ""

	if data.Live != OPWHLiveMode {
		return
	}

	if str, ok := data.Data.Obj["customer"].(string); ok {
		stripeID = str
	}

	log := fmt.Sprintf("Stripe ID: %s", stripeID)
	LogDebug(con, log)

	if e := org.GetBySubscriptionCustomerId(con, stripeID); e != nil {
		fmt.Println("Error getting customer by stripe ID")
	}

	if org.ID == "" {
		return
	}

	org.PaymentInformationNeedsUpdated = false
	org.Deactivated = false
	if se := org.Save(con); se != nil {
		fmt.Println("Error saving org.")
	}
}
func HandleCancelledSubscriptionWebhook(data stripe.Event, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	stripeID := ""

	if data.Live != OPWHLiveMode {
		return
	}

	if str, ok := data.Data.Obj["customer"].(string); ok {
		stripeID = str
	}

	log := fmt.Sprintf("Stripe ID: %s", stripeID)
	LogDebug(con, log)

	if e := org.GetBySubscriptionCustomerId(con, stripeID); e != nil {
		fmt.Println("Error getting customer by stripe ID")
	}

	if org.ID == "" {
		return
	}

	org.PaymentInformationNeedsUpdated = true
	org.Deactivated = true
	if se := org.Save(con); se != nil {
		fmt.Println("Error saving org.")
	}
}
func HandleFailedInvoiceWebhook(data stripe.Event, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	stripeID := ""

	if data.Live != OPWHLiveMode {
		return
	}

	if str, ok := data.Data.Obj["customer"].(string); ok {
		stripeID = str
	}

	log := fmt.Sprintf("Stripe ID: %s", stripeID)
	LogDebug(con, log)

	if e := org.GetBySubscriptionCustomerId(con, stripeID); e != nil {
		fmt.Println("Error getting customer by stripe ID")
	}

	if org.ID == "" {
		return
	}

	org.PaymentInformationNeedsUpdated = true
	if se := org.Save(con); se != nil {
		fmt.Println("Error saving org.")
	}
}
func HandleSuccessfulInvoiceWebhook(data stripe.Event, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	stripeID := ""

	if data.Live != OPWHLiveMode {
		return
	}

	if str, ok := data.Data.Obj["customer"].(string); ok {
		stripeID = str
	}

	log := fmt.Sprintf("Stripe ID: %s", stripeID)
	LogDebug(con, log)

	if e := org.GetBySubscriptionCustomerId(con, stripeID); e != nil {
		fmt.Println("Error getting customer by stripe ID")
	}

	if org.ID == "" {
		return
	}

	org.PaymentInformationNeedsUpdated = false
	org.Deactivated = false
	if se := org.Save(con); se != nil {
		fmt.Println("Error saving org.")
	}
}
func HandleSuccessfulDonationWebhook(data stripe.Event, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	donor := &Donor{}
	stripeID := ""

	if data.Live != OPWHLiveMode {
		return
	}

	if str, ok := data.Data.Obj["customer"].(string); ok {
		stripeID = str
	}

	log := fmt.Sprintf("Stripe ID: %s", stripeID)
	LogDebug(con, log)

	if e := donor.GetByStripeCustomerID(con, stripeID); e != nil {
		fmt.Println("Error getting customer by stripe ID")
	}

	if donor.ID == "" {
		return
	}

	donor.PaymentInformationNeedsUpdated = false
	if se := donor.Save(con); se != nil {
		fmt.Println("Error saving donor")
	}
}
func HandleFailedDonationWebhook(data stripe.Event, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	donor := &Donor{}
	stripeID := ""
	if str, ok := data.Data.Obj["customer"].(string); ok {
		stripeID = str
	}

	if data.Live != OPWHLiveMode {
		return
	}

	log := fmt.Sprintf("Stripe ID: %s", stripeID)
	LogDebug(con, log)

	if e := donor.GetByStripeCustomerID(con, stripeID); e != nil {
		fmt.Println("Error getting customer by stripe ID")
	}

	if donor.ID == "" {
		return
	}

	donor.PaymentInformationNeedsUpdated = true
	if se := donor.Save(con); se != nil {
		fmt.Println("Error saving donor")
	}

}

//Page templates
type DonationTemplate struct {
	OrgName        string
	DoantionID     string
	DonationAmount int64
	DonorName      string
	DonorEmail     string
	Phone          string
}

type MessageRequest struct {
	Body       string   `json:"body"`
	Recipients []string `json:"recipients"`
}

func ParseStripeEvent(sc stripe.Event) martini.Handler {
	return binding.Json(sc)
}

func ParseMessageRequest(m MessageRequest) martini.Handler {
	return binding.Json(m)
}

type SubscriptionPaymentSetRequest struct {
	Token 			string `json:"token"`
	Email 			string `json:"email"`
}

func ParseSubscriptionRequest(s SubscriptionPaymentSetRequest) martini.Handler {
	return binding.Json(s)
}

//Parse JSON Requests
type BankAccountRequest struct {
	Name        string `json:"name"`
	Corporation bool   `json:"corporation"` //corporate or individual
	BankAccount bool   `json:"bank"`        //bank account or debit card
	Token       string `json:"token"`
}

func ParseBankAccount(b BankAccountRequest) martini.Handler {
	return binding.Json(b)
}

type DonationRequest struct {
	Name            string `json:"name"`
	Zip             string `json:"zip"`
	Amount          int64  `json:"amount"`
	StripeCardToken string `json:"stripeCardToken"`
	Phone           string `json:"phone"`
	Email           string `json:"email"`
}

func ParseDonation(d DonationRequest) martini.Handler {
	return binding.Json(d)
}

type DonorPaymentUpdateRequest struct {
	DonorID 				string `json:"donorID"`
	Token 					string `json:"token"`
}

func ParseDonorPaymentUpdateRequest(d DonorPaymentUpdateRequest) martini.Handler {
	return binding.Json(d)
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func ParseLogin(l LoginRequest) martini.Handler {
	return binding.Json(l)
}

type SignupRequest struct {
	FirstName       string `json:"firstName"`
	LastName        string `json:"lastName"`
	DobDay          int    `json:"dobDay"`
	DobMonth        int    `json:"dobMonth"`
	DobYear         int    `json:"dobYear"`
	IsBusiness      bool   `json:"isBusiness"`
	Email           string `json:"email"`
	TextCode        string `json:"textCode"`
	Password        string `json:"password"`
	EIN             string `json:"ein"`
	SSNLastFour     string `json:"ssnLastFour"`
	TOSTimeAccepted int64  `json:"tosTimeAccepted"`
	TOSIPAccepted   string `json:"tosIPAccepted"`
	BusinessName    string `json:"businessName"`
	AddressLineOne  string `json:"addressLineOne"`
	AddressLineTwo  string `json:"addressLineTwo"`
	AddressCity     string `json:"addressCity"`
	AddressState    string `json:"addressState"`
	AddressZip      string `json:"addressZip"`
	AddressCountry  string `json:"addressCountry"`
}

func (this *SignupRequest) Validate() bool {
	if this.FirstName == "" ||
		this.LastName == "" ||
		this.DobYear == 0 ||
		this.DobDay == 0 ||
		this.DobMonth == 0 ||
		this.Email == "" ||
		this.AddressLineOne == "" ||
		this.TextCode == "" {

		return false
	}

	return true
}

func ParseSignup(s SignupRequest) martini.Handler {
	return binding.Json(s)
}

type ChangeRequest struct {
	OldPassword string `json:"oldPassword"`
	NewPassword string `json:"newPassword"`
}

func ParseChangePasswordRequest(c ChangeRequest) martini.Handler {
	return binding.Json(c)
}

type ResetRequest struct {
	Email string `json:"email"`
}

func ParseResetPasswordEmail(f ResetRequest) martini.Handler {
	return binding.Json(f)
}

// Authorization Handler
func Auth(req *http.Request, r render.Render) {
	qs := req.URL.Query()
	email := qs.Get("email")
	token := qs.Get("token")
	con := appengine.NewContext(req)
	org := &Organization{}
	if err := org.Authenticate(con, email, token); err != nil {
		LogError(con, "Authentication error", err)
		ReturnErrorUnauthorized(r)
		return
	}
}

func HandleCreateDeal(res http.ResponseWriter, r render.Render, req *http.Request) {
	con := appengine.NewContext(req)
	newDeal := &Deal{}
	if saveEr := newDeal.Save(con); saveEr != nil {
		LogError(con, "Problem Creating deal ", saveEr)
		ReturnErrorInternalServerError(r)
		return
	}
}

func HandleShortURL(r render.Render, params martini.Params, res http.ResponseWriter, req *http.Request) {
	con := appengine.NewContext(req)
	shortCode := params["dID"]
	url, er := GetURLForShortCode(con, shortCode)
	if er != nil {
		LogError(con, "No donation for short url", er)
		ReturnErrorNoRoute(r)
		return
	}
	http.Redirect(res, req, url, http.StatusMovedPermanently)

}

func HandleGetPageDonation(r render.Render, res http.ResponseWriter, req *http.Request) {
	con := appengine.NewContext(req)
	org := &Organization{}
	donation := &Donation{}
	qs := req.URL.Query()
	donationID := qs.Get("dID")
	orgID := qs.Get("oID")
	if errIDGet := org.GetByID(con, orgID); errIDGet != nil {
		LogError(con, "Error getting organization by ID", errIDGet)
		ReturnErrorInternalServerError(r)
		return
	}
	if err := donation.GetByID(con, donationID); err != nil {
		LogError(con, "Error getting donation by ID", err)
		ReturnErrorInternalServerError(r)
		return
	}
	//serve html page with donation in it
	postRoute := fmt.Sprintf("%sapi/v2/donation/%s/%s", OPBaseURL, orgID, donationID)

	if !donation.Completed {
		f := map[string]interface{}{
			"OrgName":           org.Name,
			"DonationID":        donationID,
			"DonationAmount":    donation.Amount,
			"DonorName":         donation.Name,
			"DonorEmail":        donation.Email,
			"DonorNumber":       donation.Phone,
			"PostURL":           postRoute,
			"DonationCompleted": donation.Completed,
		}
		r.HTML(200, "donate", f)
	} else {

		f := map[string]interface{}{
			"OrgName":           org.Name,
			"DonorName":         donation.Name,
			"DonationCompleted": donation.Completed,
			"DonationAmount":    donation.Amount,
			"DonationTimestamp": donation.Timestamp,
			"PostURL":           postRoute,
		}
		r.HTML(200, "donate", f)
	}
}

func HandleGetPageDonorUpdate(r render.Render, res http.ResponseWriter, req *http.Request) {
	con := appengine.NewContext(req)
	donor := &Donor{}
	qs := req.URL.Query()
	donorID := qs.Get("drID")
	println(donorID)
	if err := donor.GetByID(con, donorID); err != nil {
		LogError(con, "Error getting donor by ID", err)
		ReturnErrorInternalServerError(r)
		return
	}
	f := map[string]interface{}{
		"DonorID" : donor.ID,
		"DonorUpdate" : true,
	}
	r.HTML(200, "donate", f)

}

func HandleBankInfo(data BankAccountRequest,
	params martini.Params,
	res http.ResponseWriter,
	req *http.Request,
	r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	qs := req.URL.Query()
	email := qs.Get("email")
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting oganization by Email", err)
		ReturnErrorUnauthorized(r)
		return
	}
	if _, err := org.AddBankAccount(con, data.Token); err != nil {
		LogError(con, "Error adding bank account info", err)
		ReturnErrorAddingBankAccount(r)
		return
	}
}

//Route Handlers
func HandlePruneDonations(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	serverKey := params["serverKey"]
	if serverKey == OPServerKey {
		PruneOldDonations(con)
	}
}

func HandleAccountTransfers(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	return
}

type DonationReturn struct {
	Name      string
	Amount    int64
	Timestamp int64
	Method    string
	Email     string
	Phone     string
}

func HandleGetDonations(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	qs := req.URL.Query()
	email := qs.Get("email")
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting organization by Email", err)
		ReturnErrorUnauthorized(r)
		return
	}
	donations, errDonations := org.GetAllDonations(con)
	if errDonations != nil {
		LogError(con, "Error getting all donations for org", errDonations)
		ReturnErrorInternalServerError(r)
		return
	}

	toReturn := []*DonationReturn{}
	for _, dObj := range donations {
		dateTime := dObj.Created
		if dateTime == 0 {
			dateTime = dObj.Timestamp
		}

		r := &DonationReturn{
			Name:      dObj.Name,
			Email:     dObj.Email,
			Phone:     dObj.Phone,
			Amount:    dObj.Amount,
			Method:    dObj.Method,
			Timestamp: dateTime,
		}
		toReturn = append(toReturn, r)
	}

	r.JSON(200, toReturn)
}

func HandleGetDonation(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	donationID := params["donationID"]

	donation := &Donation{}
	er := donation.GetByID(con, donationID)

	if er != nil || donation.Completed == true {
		LogError(con, "Invalid donation ID", er)
		ReturnInvalidDonationError(r)
		return
	}

	r.JSON(200, donation)
}

// Message Handlers
//HTTP PUT: Edit message draft
//HTTP DELETE: Delete message draft

// m.Put("/api/v1/message/:mID", Auth, ParseMessageRequest(MessageRequest{}), HandlePutMessage)

func HandleGetMessage(params martini.Params, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	qs := req.URL.Query()
	email := qs.Get("email")
	mID := qs.Get("id")

	// Get org
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting oganization by Email", err)
		ReturnErrorUnauthorized(r)
		return
	}

	if mID == "" {
		messages, er := org.GetAllMessages(con)
		if er != nil {
			LogError(con, "Error getting all messages", er)
			ReturnErrorInternalServerError(r)
			return
		}
		r.JSON(200, messages)

	} else {
		message := &Message{}
		getEr := message.GetByID(con, mID)
		if getEr != nil {
			LogError(con, "Error getting message", getEr)
			ReturnErrorInternalServerError(r)
			return
		}
		r.JSON(200, message)
	}
}

func HandlePutMessage(data MessageRequest, params martini.Params, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	qs := req.URL.Query()
	email := qs.Get("email")

	// Get org
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting oganization by Email", err)
		ReturnErrorUnauthorized(r)
		return
	}

	mID := params["mID"]
	con.Debugf("MID: %s", mID)
	message := &Message{}
	getEr := message.GetByID(con, mID)
	if getEr != nil {
		LogError(con, "Error getting message", getEr)
		ReturnErrorInternalServerError(r)
		return
	}

	if data.Body != "" {
		message.Body = data.Body
	}
	if len(data.Recipients) > 0 {
		message.Recipients = data.Recipients
	}

	mesSaveEr := message.Save(con)
	if mesSaveEr != nil {
		LogError(con, "Error saving message", mesSaveEr)
		ReturnErrorInternalServerError(r)
		return
	}

}

func HandleSendMessage(params martini.Params, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	qs := req.URL.Query()
	email := qs.Get("email")

	// Get org
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting oganization by Email", err)
		ReturnErrorUnauthorized(r)
		return
	}

	mID := params["mID"]
	message := &Message{}
	getEr := message.GetByID(con, mID)
	if getEr != nil {
		LogError(con, "Error getting message", getEr)
		ReturnErrorInternalServerError(r)
		return
	}
	if message.Live {
		con.Errorf("Cannot resend a live message")
		ReturnErrorUnauthorized(r)
		return
	}
	sendEr := message.Send(con)
	if sendEr != nil {
		LogError(con, "Error sending message after put req", sendEr)
		ReturnErrorInternalServerError(r)
		return
	}

}

func HandlePostMessage(data MessageRequest, params martini.Params, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	qs := req.URL.Query()
	email := qs.Get("email")
	con.Debugf("email: %s", email)
	// Get org
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {

		LogError(con, "Error getting oganization by Email", err)
		ReturnErrorUnauthorized(r)
		return
	}

	con.Debugf("message data obj: %+v", data)

	//Make Message
	message := &Message{
		Recipients:    data.Recipients,
		Body:          data.Body,
		Live:          false,
		ResponseCount: 0,
		OrgID:         org.ID,
	}
	saveEr := message.Save(con)

	if saveEr != nil {
		LogError(con, "Error saving message", saveEr)
		ReturnErrorInternalServerError(r)
		return
	}

	r.JSON(200, message)
}

// Contact Handlers

type ContactReturn struct {
	Name  string `json:"name"`
	Phone string `json:"phone"`
	ID    string `json:"id"`
}

func HandleDeleteContact(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	qs := req.URL.Query()
	email := qs.Get("email")
	con := appengine.NewContext(req)
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting oganization by ID", err)
		ReturnErrorInternalServerError(r)
		return
	}
	contactID := params["contactID"]

	contact := &Contact{}
	er := contact.GetByID(con, contactID)

	if er != nil || contact.OrgID != org.ID {
		if contact.OrgID != org.ID {
			LogDebug(con, "Contact exists but under different account")
		} else {
			LogError(con, "Invalid contact ID", er)
		}

		ReturnInvalidContactError(r)
		return
	}

	delEr := contact.Delete(con)
	if delEr != nil {
		ReturnDeleteContactError(r)
		return
	}

	r.JSON(200, nil)
}
func HandleGetContact(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	qs := req.URL.Query()
	email := qs.Get("email")
	con := appengine.NewContext(req)
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting oganization by ID", err)
		ReturnErrorInternalServerError(r)
		return
	}
	contactID := params["contactID"]

	contact := &Contact{}
	er := contact.GetByID(con, contactID)

	if er != nil || contact.OrgID != org.ID {
		if contact.OrgID != "" && contact.OrgID != org.ID {
			LogDebug(con, "Contact exists but under different account")
		} else {
			LogError(con, "Invalid contact ID", er)
		}

		ReturnInvalidContactError(r)
		return
	}

	toReturn := &ContactReturn{
		Name:  contact.Name,
		Phone: contact.Phone,
		ID:    contact.ID,
	}

	r.JSON(200, toReturn)
}

func HandleGetContacts(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	qs := req.URL.Query()
	email := qs.Get("email")
	onlyImportedContacts := qs.Get("onlyImported")
	con := appengine.NewContext(req)
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting oganization by ID", err)
		ReturnErrorInternalServerError(r)
		return
	}

	toReturn := make(map[string][]*ContactReturn)

	contacts, errContacts := org.GetAllContacts(con)
	if errContacts != nil {
		LogError(con, "Error getting all contacts for org", errContacts)
		ReturnErrorInternalServerError(r)
	}

	importedReturnContacts := []*ContactReturn{}
	for _, contact := range contacts {
		iC := &ContactReturn{
			Name:  contact.Name,
			Phone: contact.Phone,
			ID:    contact.ID,
		}
		importedReturnContacts = append(importedReturnContacts, iC)
	}
	toReturn["imported"] = importedReturnContacts

	if onlyImportedContacts != "true" {
		donors, errDonors := org.GetAllDonors(con)
		if errDonors != nil {
			LogError(con, "Error getting all donors for org", errDonors)
			ReturnErrorInternalServerError(r)
		}

		donorContacts := []*ContactReturn{}
		for _, donor := range donors {
			if donor.Phone != "" {
				iC := &ContactReturn{
					Name:  donor.Name,
					Phone: donor.Phone,
					ID:    donor.ID,
				}
				donorContacts = append(donorContacts, iC)
			}
		}
		toReturn["donors"] = donorContacts
	}

	r.JSON(200, toReturn)

}

func HandleGetDonors(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	qs := req.URL.Query()
	email := qs.Get("email")
	con := appengine.NewContext(req)
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting oganization by ID", err)
		ReturnErrorInternalServerError(r)
		return
	}
	donors, errDonors := org.GetAllDonors(con)
	if errDonors != nil {
		LogError(con, "Error getting all donations for org", errDonors)
		ReturnErrorInternalServerError(r)
	}
	r.JSON(200, donors)

}

func HandleTwilioWebHook(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	qs := req.URL.Query()
	fromNumber := qs.Get("From")
	body := qs.Get("Body")
	accountSid := qs.Get("AccountSid")
	LogInfo(con, fmt.Sprintf("%s %s %s", fromNumber, body, accountSid))
	if accountSid == OPTwilioSID {
		LogInfo(con, "Entering Repsonder...")
		SMSRespond(con, res, req, fromNumber, body, accountSid)
	} else {
		LogError(con, "Twilio Account SID does not match OP SID", fmt.Errorf("Twilio Account SID does not match OP SID"))
		ReturnErrorInvalidOranization(r)
		return
	}
}

func HandleWebDonation(data DonationRequest, params martini.Params, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	LogDebug(con, fmt.Sprintf("%+v", data))
	//Get Organization by ID
	orgID := params["orgID"]

	organization := &Organization{}
	if err := organization.GetByID(con, orgID); err != nil {
		LogError(con, "Error getting oganization by ID", err)
		ReturnErrorInternalServerError(r)
		return
	}

	//Get Donation by ID
	donationID := params["donationID"]
	donation := &Donation{}
	if err := donation.GetByID(con, donationID); err != nil {
		LogError(con, "Error getting donation by ID", err)
		ReturnErrorInternalServerError(r)
		return
	}

	//Update donation with request
	if err := donation.UpdateWithRequest(con, data); err != nil {
		LogError(con, "Error updating donation with request", err)
		ReturnErrorInternalServerError(r)
		return
	}

	//Try getting by email
	email := data.Email
	donor := &Donor{}
	if err := donor.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting donor by Email", err)
		ReturnErrorInternalServerError(r)
		return
	}
	//No donor with that email – create one
	if donor.ID == "" {
		donor.Email = data.Email
		donor.Name = data.Name
		donor.Phone = data.Phone
		donor.PhoneVerified = donation.PhoneVerified
		donor.Zip = data.Zip
		if saveErr := donor.Save(con); saveErr != nil {
			LogError(con, "Error saving new donor", saveErr)
			ReturnErrorInternalServerError(r)
			return
		}
	}

	//Assign donation to the donor
	if donation.Method == "" {
		donation.Method = OPDonationMethodWeb
	}
	donation.DonorID = donor.ID
	if err := donation.Save(con); err != nil {
		LogError(con, "Error saving the donation after updating from request and assigning donor id", err)
		ReturnErrorInternalServerError(r)
		return
	}

	//Charge the donor with stripe card token
	if err := donor.ChargeWebCustomer(con, data.StripeCardToken, donation); err != nil {
		LogError(con, "Error charging card token for donation", err)
		ReturnErrorInternalServerError(r)
		return
	}

	r.JSON(200, map[string]interface{}{
		"org":       organization.Name,
		"name":      donor.Name,
		"amount":    donation.Amount,
		"timestamp": donation.Timestamp})
}

func HandleDonorPaymentUpdateRequest(data DonorPaymentUpdateRequest, params martini.Params, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	donorID := data.DonorID
	donor := &Donor{}
	if err := donor.GetByID(con, donorID); err != nil {
		LogError(con, "Error getting donor by ID", err)
		ReturnErrorInternalServerError(r)
		return
	}
	if err := donor.UpdateCustomerPaymentInfo(con, data.Token); err != nil {
		LogError(con, "Error updating payment info of the donor", err)
		ReturnErrorInternalServerError(r)
		return
	}
}

func HandleDonationPageRedirect(params martini.Params, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)

	//get org from id
	orgTextCode := params["orgTextCode"]
	organization := &Organization{}
	if err := organization.GetByTextCode(con, orgTextCode); err != nil {
		LogError(con, "Error looking up organization by text code", err)
		ReturnErrorInvalidOranization(r)
		return
	}

	//create a donation
	donation := &Donation{}
	donation.OrgID = organization.ID
	donation.Amount = 0
	donation.PhoneVerified = false
	if saveErr := donation.Save(con); saveErr != nil {
		LogError(con, "Error saving donation", saveErr)
		ReturnErrorInvalidOranization(r)
		return
	}

	//redirect to edit donation route w/ donation id
	redirectURL := fmt.Sprintf("/donation?dID=%s&oID=%s", donation.ID, organization.ID)
	http.Redirect(w, req, redirectURL, http.StatusMovedPermanently)
}

func HandleChangePassword(data ChangeRequest, params martini.Params, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	email := params["email"]
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error gettingorganization", err)
		ReturnErrorUnauthorized(r)
		return
	}
	if err := org.ChangePassword(data.OldPassword, data.NewPassword); err != nil {
		LogError(con, "Error changing password", err)
		ReturnErrorPasswordChange(r)
		return
	}
	if err := org.Save(con); err != nil {
		LogError(con, "Error saving pw change", err)
		ReturnErrorSavingAccount(r)
		return
	}
}

func HandleLogin(data LoginRequest, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	token, err := org.Login(con, data.Email, data.Password)
	if err != nil {
		LogError(con, "Login Problem "+data.Email, err)
		LogDebug(con, fmt.Sprintf("Body:\n %+v", data))
		ReturnErrorLogin(r)
		return
	}
	LogInfo(con, token)
	r.JSON(200, org.PublicJSON())
}

func HandleSendResetMessage(data ResetRequest, params martini.Params, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	if err := org.GetByEmail(con, data.Email); err != nil {
		//TODO: Log Error
		LogError(con, "Problem getting account for email: "+data.Email, err)
		ReturnErrorGettingAccountByEmail(r)
		return
	}

	newPassword, err := org.ResetPassword(con)
	if err != nil {
		LogError(con, "There was a problem generating the reset token", err)
		ReturnErrorInternalServerError(r)
		return
	}
	message := "Hello,\n\n Your Omnipledge password has been reset.\n\n New Password: " + newPassword
	LogDebug(con, message)
	if err := send(req, OPEmailAddress, org.Email, "Your password has been reset", message); err != nil {
		// There was a problem sending the email
		LogError(con, "There was a problem sending the email", err)
		ReturnErrorSendingEmail(r)
		return
	}
}

func HandleConfirmation(params martini.Params, res http.ResponseWriter, req *http.Request, r render.Render) {
	token := params["token"]
	email := params["email"]
	con := appengine.NewContext(req)
	if token == "" || email == "" {
		LogError(con, "Could not read the organization id from the params", nil)
		ReturnErrorValidation(r, "Invalid URL Parameters")
		r.HTML(200, "404", nil)
	}
	org := &Organization{}
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "GAE: Problem getting account by email.", err)
		ReturnErrorGettingAccountByEmail(r)
		r.HTML(200, "404", nil)
	}
	match, e := org.CheckConfirmationCode(con, token)
	if e != nil {
		LogError(con, "Problem matching confirmation code.", e)
		ReturnErrorMatchingConfirmationCode(r)
		r.HTML(200, "404", nil)
	} else if match {
		org.Verified = true
		if err := org.Save(con); err != nil {
			LogError(con, "GAE: Problem saving account", err)
			ReturnErrorSavingAccount(r)
			r.HTML(200, "404", nil)
		}
		url := fmt.Sprintf("%sadmin.html", OPBaseURL)
		http.Redirect(res, req, url, http.StatusMovedPermanently)
	}
}

func HandleSignup(data SignupRequest, w http.ResponseWriter, req *http.Request, r render.Render) {
	//Check for account with specified email
	con := appengine.NewContext(req)
	org := &Organization{}

	data.TOSIPAccepted = req.RemoteAddr
	data.TOSTimeAccepted = time.Now().Unix()

	LogInfo(con, "In signup handler...")
	LogDebug(con, fmt.Sprintf("New Account Request:\n%+v", data))

	if !data.Validate() {
		ReturnErrorValidation(r, "Please fill in each field.")
		return
	}

	if strings.Contains(data.TextCode, " ") {
		ReturnErrorValidation(r, "Text code cannot contain spaces.")
		return
	}

	if pValEr := ValidatePword(data.Password); pValEr != nil {
		ReturnErrorValidation(r, pValEr.Error())
		return
	}

	// If there is a problem with GAE don't continue signup.
	// Note: org is more or less a sanity check, NOT a valid email check
	if er := org.GetByEmail(con, data.Email); er != nil {
		LogError(con, "GAE: Problem getting account by email.", er)
		ReturnErrorGettingAccountByEmail(r)
		return
	}

	// If an organization is already using the email, it will show here
	if org.Email != "" {
		ReturnErrorEmailInUse(r)
		return
	}

	valueCodes := []string{"race",
		"cure",
		"fight",
		"checkup",
		"cancer",
		"save",
		"cost",
		"demo",
		"can",
		"donate",
		"cross",
		"give",
		"omni",
		"pure",
		"me",
		"family",
		"hope",
		"change",
		"sam",
		"ricky",
		"swag",
		"fuck",
		"shit",
	}

	for _, s := range valueCodes {
		if strings.ToLower(data.TextCode) == s {
			LogError(con, "Text code is already in use", fmt.Errorf("Text code is on reserved list"))
			ReturnErrorTextCodeInUse(r)
			return
		}
	}

	if err := org.GetByTextCode(con, data.TextCode); err == nil {
		LogError(con, "Text code is already in use", err)
		ReturnErrorTextCodeInUse(r)
		return
	}

	//Create an org object
	org.Email = data.Email
	org.ContactFirstName = data.FirstName
	org.ContactLastName = data.LastName
	org.IsBusiness = data.IsBusiness
	org.TextCode = data.TextCode
	org.EIN = "" /// data.EIN

	//Generate Password Hash
	if err := org.NewPassword(data.Password); err != nil {
		LogError(con, "Problem generating password hash.", err)
		ReturnErrorValidation(r, fmt.Sprint(err))
		return
	}

	managedAcct, err := org.CreateManagedAccount(con, data)
	if err != nil {
		LogError(con, "Problem creating managed account", err)
		ReturnErrorValidation(r, fmt.Sprint(err))
		return
	}

	org.SetAccountMangedAccountFields(con, managedAcct)

	//Save object
	if err := org.Save(con); err != nil {
		LogError(con, "Problem saving org on signup", err)
		ReturnErrorSavingAccount(r)
		return
	}

	//Create a confirmation code
	confirmationCode, err := org.GenerateConfirmationCode(con)
	if err != nil {
		LogDebug(con, "Signup: Problem creating a confirmation code")
		LogError(con, "Problem generating confirmation code.", err)
		ReturnErrorInternalServerError(r)
		return
	}

	//Save object
	if err := org.Save(con); err != nil {
		LogError(con, "Problem saving org on signup", err)
		ReturnErrorSavingAccount(r)
		return
	}
	confURL := fmt.Sprintf("%sapi/v2/account/%s/%s/confirm", OPBaseURL,
		url.QueryEscape(org.Email),
		url.QueryEscape(confirmationCode))
	LogInfo(con, confURL)
	message := "Thanks for signing up for Omnipledge! Please click the link below to confirm your email.\n\n " + confURL
	if err := send(req, OPEmailAddress, org.Email, "Omnipledge Account Confirmation", message); err != nil {
		// There was a problem sending the email
		LogError(con, "Problem sending confirmation email", err)
		ReturnErrorInternalServerError(r)
		return
	}
	LogDebug(con, message)
	r.JSON(200, org.PublicJSON())

	//return
}

// Subscription payment handlers
func HandleSetSubscriptionPayment(data SubscriptionPaymentSetRequest, w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	qs := req.URL.Query()
	email := qs.Get("email")
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting organization by Email", err)
		ReturnErrorUnauthorized(r)
		return
	}
	if _, err := org.SubscribeWithPayment(con, data.Token); err != nil {
		LogError(con, "Error creating organization subscription", err)
		ReturnErrorCreatingSubscription(r)
		return
	}
	r.JSON(200, map[string]interface{}{
		"status": true,
		"planSubscriptionId": org.PlanSubscriptionId,
	})
}

func HandleCancelSubscription(w http.ResponseWriter, req *http.Request, r render.Render) {
	con := appengine.NewContext(req)
	org := &Organization{}
	qs := req.URL.Query()
	email := qs.Get("email")
	if err := org.GetByEmail(con, email); err != nil {
		LogError(con, "Error getting organization by Email", err)
		ReturnErrorUnauthorized(r)
		return
	}
	err := org.CancelSubscription(con)
	if err != nil {
		LogError(con, "Error canceling organization subscription", err)
		ReturnErrorCancelingSubscription(r)
	}
}

func StripeOPClient(c appengine.Context) *client.API {
	/// use on app engine
	// httpClient := urlfetch.Client(c)

	/// use on local
	tr := &http.Transport{
    TLSClientConfig: &tls.Config{ MinVersion: 1 },
    DisableCompression: true,
	}
	httpClient := &http.Client{Transport: tr}

	stripeClient := client.New(OPStripeAPI, stripe.NewBackends(httpClient))
	return stripeClient
}

//App Engine Logging Convenience
func send(r *http.Request, from, to, subject, body string) error {
	c := appengine.NewContext(r)
	return SendEmail(c, from, to, subject, body)
}

func SendEmail(c appengine.Context, from, to, subject, body string) error {
	msg := &mail.Message{
		Sender:  from,
		To:      []string{to},
		Subject: subject,
		Body:    body,
	}
	if err := mail.Send(c, msg); err != nil {
		return err
	}
	fmt.Printf("Email sent successfully to :%s\nWith Message: %s\n", to, body)
	return nil
}

func GenerateHMACUserID(userID string) string {
	//Intercom key
	secret := "YmKUfDEvmTQZnd6a2MWZNFXKGOfrS8YXWWs8UK0i"
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(userID))
	return hex.EncodeToString(h.Sum(nil))
}

// https://developer.mozilla.org/en-US/docs/HTTP/Access_control_CORS
// "preflight" Options request
func AddHeaders(res http.ResponseWriter) {
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,HEAD,OPTIONS")
	res.Header().Set("Access-Control-Allow-Headers", "Content-Type,x-requested-with")
	res.Header().Set("Content-Type", "application/json")
}
