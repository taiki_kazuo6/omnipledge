'use strict';

angular.module('omnipledge.public')
  .controller('SubscriptionCtrl', function ($scope, $rootScope, Subscription, $location) {

    $scope.submitting = false;
    $scope.cancelling = false;

    $scope.submitPaymentForm = function() {
      $scope.submitting = true;
      Subscription.setPaymentCardInfo({
        formId: 'subscription-payment-form',
      })
      .then(function() {
        $scope.submitting = false;
      })
      .catch(function() {
        $scope.submitting = false;
      });
    }
  });
