'use strict';

angular.module('omnipledge.public')
  .controller('SignupCtrl', function ($scope, $location, Account) {
    $scope.result = '';
    $scope.loading = false;
    $scope.signedUp = false;
    $scope.triedSignup = false;

    $scope.newUser = {
      firstName: "",
      lastName: "",
      email: "",
      isBusiness: "",
      password: "",
      textCode: "",
      addressLineOne: "",
      addressLineTwo: "",
      addressCity: "",
      addressState: "",
      addressZip: "",
      addressCountry: "",
      /* test values */
      // firstName: "John",
      // lastName: "Doe",
      // email: "tester1@gmail.com",
      // isBusiness: false,
      // password: "abcde123",
      // textCode: "SOME",
      // addressLineOne: "350 5th Avenue",
      // addressLineTwo: "",
      // addressCity: "New York",
      // addressState: "NY",
      // addressZip: "10110",
      // addressCountry: "US",
      // birthday: "1989-01-10",
    };

    $scope.signup = function(user, form){
      $scope.triedSignup = true;
      if (!form.$valid) {
        console.log(form);
        console.log('Form validation failed');
        return;
      }
      if(!$scope.signedUp || !$scope.loading){
        $scope.loading = true;

        Account.signup(user).then(function(){
          $scope.error = false;
          $scope.loading = false;
          $scope.signedUp = true;
          $scope.result = 'Registration successful. You\'ll be redirected to login page in moments.';
          setTimeout(function() {
            window.location.href = '/admin.html#/app/login/register-success';
          }, 700);
        },function(error){
          $scope.loading = false;
          $scope.error = true;
          $scope.result = error;
        });
      }
    }
  });
