'use strict';

angular.module('omnipledge.public')
  .controller('ForgotCtrl', function ($scope, Account) {

    $scope.email = '';
    $scope.message = '';
    $scope.error = false;
    $scope.loading = false;

    $scope.reset = function(email){
      $scope.loading = true;
      Account.resetPassword(email).then(function(){
        $scope.email = '';
        $scope.loading = false;
        $scope.error = false;
        $scope.message = 'An email with reset instructions has been sent.';

      },function(errorMessage){
        $scope.loading = false;
        $scope.error = true;
        $scope.message = errorMessage;
      });

    }



  });
