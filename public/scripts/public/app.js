'use strict';

angular.module('omnipledge.public', [
  'ngRoute',
  'omnipledge.services',
  'angulartics',
  'angulartics.segment.io'
])
  .config(function ($routeProvider, $analyticsProvider) {

    $analyticsProvider.virtualPageviews(true);

    $routeProvider
      .when('/', {
        templateUrl: 'views/public/home.html',
        controller: 'HomeCtrl'
      })
      .when('/signup', {
        templateUrl: 'views/public/signup.html',
        controller: 'SignupCtrl'
      })
      .when('/features', {
        templateUrl: 'views/public/features.html',
        controller: 'FeaturesCtrl'
      })
      .when('/how', {
        templateUrl: 'views/public/how.html',
        controller: 'HowItWorksCtrl'
      })
      .when('/forgot-password', {
        templateUrl: 'views/public/forgot-password.html',
        controller: 'ForgotCtrl'
      })
      .when('/terms', {
        templateUrl: 'views/public/tos.html'
      })
      .when('/help', {
        templateUrl: 'views/public/help.html'
      })
      .when('/subscription', {
        templateUrl: 'views/public/subscription.html',
        controller: 'SubscriptionCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
