'use strict';

angular.module('omnipledge.services')
  .service('Account', function Account(API, $http, $log, $q, Validator, $cookieStore, GateKeeper) {
    var Account = {};
    var validator = new Validator();

    Account.token = null;
    Account.email = null;
    Account.userName = null;
    Account.textCode = null;
    Account.isAuthenticated = false;
    Account.bankAccount = null;
    Account.timestamp = null;
    Account.userHash = null;
    Account.planSubscriptionId = null;

    GateKeeper.onLock(function() {
      Account.loadLoginDetails();
    });

    Account.logout = function(){

      Account.token = null;
      Account.email = null;
      Account.name = null;
      Account.textCode = null;
      Account.userName = null;
      Account.isAuthenticated = false;
      Account.bankAccount = null;
      Account.timestamp = null;
      Account.userHash = null;

      $cookieStore.put('account', null);
      GateKeeper.lock();

    };

    Account.get = function(key) {
      if (!Account[key]) {
        Account.loadLoginDetails();
      }
      return Account[key];
    };

    Account.set = function(key, value) {
      Account[key] = value;
      Account.saveLoginDetails();
      return value;
    }

    Account.authParams = function() {
      return '?email=' + Account.get('email') + '&token=' + Account.get('token');
    };

    Account.loadLoginDetails = function() {
      var account = $cookieStore.get('account');
      if (!account) {
        return;
      }
      Account.email = account.email;
      Account.name = account.name;
      Account.textCode = account.textCode;
      Account.userName = account.userName;
      Account.bankAccount = account.bankAccount;
      Account.timestamp = account.timestamp;
      Account.userHash = account.userHash;
      Account.planSubscriptionId = account.planSubscriptionId;
      Account.token = window.localStorage.getItem('op-token');
      if (Account.token) {
        Account.isAuthenticated = true;
      }
    }

    Account.saveLoginDetails = function() {
      $cookieStore.put('account', Account);
      window.localStorage.setItem('op-token', Account.token);
    }

    Account.addedBankAccount = function(){
      Account.bankAccount = true;
      $cookieStore.put('account', Account);
    };

    Account.removedBankAccount = function(){
      Account.bankAccount = false;
      $cookieStore.put('account', Account);
    };

    function loginSuccess(data, promise) {
      $log.info('omnipledge.services:Account+login | Successful login.');

      Account.name = data['name'];
      Account.userName = data['userName'];
      Account.textCode = data['textCode'];
      Account.email = data['email'];
      Account.token = data['token'];
      Account.isAuthenticated = true;
      Account.bankAccount = data['bankAccount'];
      Account.timestamp = data['timestamp'];
      Account.userHash = data['hmac'];
      Account.planSubscriptionId = data['planSubscriptionId']

      Account.saveLoginDetails();

      GateKeeper.unlock();

      promise.resolve();
    }

    Account.login = function(email, password){
      var deferred = $q.defer();

      /*if(Account.isAuthenticated){
        $log.info('omnipledge.services:Account+login | Ignoring login request, user is already authenticated.');
        deferred.resolve();
      }
      else */
      if(validator.isEmail(email) && validator.isPassword(password)){
        $log.debug('omnipledge.services:Account+login | Logging in');

        var request_def = {method: 'POST', url: API.ACCOUNTS_LOGIN, data:{
          "email":email,
          "password":password
        }};

        $http(request_def).
          success(function(data) {
            loginSuccess(data, deferred);
          }).
          error(function(data) {
            Account.isAuthenticated = false;
            $log.warn('omnipledge.services:Account+login | Login failed:');
            $log.warn('omnipledge.services:Account+login |'+data.toString());
            deferred.reject(data['userMessage']);
          });
      }
      else if(!validator.isEmail(email)){
        deferred.reject('Invalid email');
      }
      else if(!validator.isPassword(password)){
        deferred.reject('Invalid password');
      }

      return deferred.promise;
    };

    Account.signup = function(model){
      var deferred = $q.defer();
      var name = model.firstName + ' ' + model.lastName;

      if(Account.isAuthenticated){
        $log.warn('omnipledge.services:Account+signup | Signup when a user is already authenticated.');
        deferred.reject('You seem to already be logged in!');
      }
      else if(validator.isPassword(model.password)&&
        validator.isEmail(model.email)&&
        validator.isName(name)&&
        validator.isTextCode(model.textCode)){

        $log.debug('omnipledge.services:Account+signup | Creating new account');

        // birthday calculation
        var bday = new Date(model.birthday);
        model.dobDay = bday.getDate();
        model.dobMonth = bday.getMonth() + 1;
        model.dobYear = bday.getFullYear();

        var request_def = {method: 'POST', url: API.ACCOUNTS, data: model};

        $http(request_def).
          success(function(data) {
            loginSuccess(data, deferred);
          }).
          error(function(data) {
            Account.isAuthenticated = false;
            $log.warn('omnipledge.services:Account+signup | Signup failed:');
            $log.warn('omnipledge.services:Account+signup |'+angular.toJson(data));
            deferred.reject(data['userMessage']);
          });
      }
      else{
        $log.warn('omnipledge.services:Account+signup | Signup validation failed!');
        if(!validator.isPassword(model.password)){
          deferred.reject('Make sure your password is more than 6 characters long and less than 32 characters long.');

        }else if(!validator.isEmail(model.email)){
          deferred.reject('Please enter a valid email.');

        }else if(!validator.isName(name)){
          deferred.reject('Please enter your name.');

        }else if(!validator.isTextCode(model.textCode)){
          deferred.reject('Make sure your text code is only numbers and letters - no spaces or special characters please.');

        }else{
          deferred.reject('It looks like that isn\'t quite right. Please try again.');
        }
      }

      return deferred.promise;
    };

    Account.changePassword = function(oldPassword, newPassword){
      var deferred = $q.defer();

      if(Account.isAuthenticated && validator.isPassword(oldPassword) && validator.isPassword(newPassword)){
        $log.debug('omnipledge.services:Account+changePassword | Changing password');

        var request_def = {method: 'POST', url: API.ACCOUNTS_CHANGE_PASSWORD, data:{
          "oldPassword":oldPassword,
          "newPassword":newPassword
        }};

        $http(request_def).
          success(function() {
            $log.info('omnipledge.services:Account+changePassword | Successful password change.');
            deferred.resolve();
          }).
          error(function(data) {
            $log.warn('omnipledge.services:Account+changePassword | Password change failed:');
            $log.warn('omnipledge.services:Account+changePassword |'+data.toString());
            deferred.reject(data['userMessage']);
          });
      }

      else {
        $log.error('omnipledge.services:Account+changePassword | Cannot change password when user is not authenticated.');
        deferred.reject('You can\'t change your password if you\'re not logged in!');
      }

      return deferred.promise;
    };

    Account.resetPassword = function(email){
      var deferred = $q.defer();
      if(Account.isAuthenticated){
        $log.warn('omnipledge.services:Account+resetPassword | Resetting password even though the user is authenticated?');
      }
      else{
        $log.debug('omnipledge.services:Account+resetPassword | Resetting password');
      }

      var request_def = {method: 'POST', url: API.ACCOUNTS_RESET, data: { "email":email }};

      $http(request_def).
        success(function() {
          $log.info('omnipledge.services:Account+resetPassword | Successful password reset request.');
          deferred.resolve();
        }).
        error(function(data) {
          $log.warn('omnipledge.services:Account+resetPassword | Password reset failed:');
          $log.warn('omnipledge.services:Account+resetPassword |'+data.toString());
          deferred.reject(data['userMessage']);
        });
      return deferred.promise;
    };

    return Account;
  });


