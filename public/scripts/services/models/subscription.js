'use strict';

angular.module('omnipledge.services')
  .factory('Subscription', function($http, $log, Account, $q, API) {
    var Subscription = function(){};

    Subscription.setPaymentCardInfo = function(params){
      var deferred = $q.defer();

      function stripeResponseHandler(status, response) {
        if (response.error) {
          $log.error('omnipledge.services:Subscription+setPaymentCardInfo | Stripe error getting payment card token');
          $log.error('omnipledge.services:Subscription+setPaymentCardInfo | Error message ' + angular.toJson(response['error']));
          deferred.reject();
        } else {
          $log.debug('omnipledge.services:Subscription+setPaymentCardInfo | Payment card token acquired');
          $log.debug('omnipledge.services:Subscription+setPaymentCardInfo | Sending to back end');

          var url = API.SUBSCRIPTION_SET_PAYMENT_INFO + Account.authParams();
          var request_def = {
            "method": 'POST',
            url: url,
            data: {
              "token": response['id'],
            }
          };

          $http(request_def).
            success(function(res) {
              $log.info('omnipledge.services:Subscription+setPaymentCardInfo | Successful saving payment card token');
              deferred.resolve(res);
            }).
            error(function(data) {
              $log.error('omnipledge.services:Subscription+setPaymentCardInfo | Error saving payment card token');
              $log.error('omnipledge.services:Subscription+setPaymentCardInfo | Error message ' + data['userMessage']);
              deferred.reject(data['userMessage']);
            });
        }
      }

      var form = document.getElementById(params.formId);
      Stripe.card.createToken(form, stripeResponseHandler);

      return deferred.promise;
    };

    Subscription.cancel = function() {
      var deferred = $q.defer();

      var url = API.SUBSCRIPTION_CANCEL + Account.authParams();
      var request_def = {
        "method": 'POST',
        url: url,
      };

      $http(request_def).
        success(function() {
          $log.info('omnipledge.services:Subscription+cancel | Successful canceling subscription');
          deferred.resolve();
        }).
        error(function(data) {
          $log.error('omnipledge.services:Subscription+cancel | Error canceling subscription');
          $log.error('omnipledge.services:Subscription+cancel | Error message ' + data['userMessage']);
          deferred.reject(data['userMessage']);
        });

      return deferred.promise;
    }

    return Subscription;
  });
