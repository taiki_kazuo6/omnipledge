'use strict';

angular.module('omnipledge.services')
  .factory('Validator', function() {
    var Validator = function(){};

    Validator.prototype.isEmail = function(email){
     if(angular.isDefined(email) &&
        angular.isString(email)){
       var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
       return re.test(email);
     }
      else return false;
    };

    Validator.prototype.isPassword = function(password){
      return (angular.isDefined(password) &&
              angular.isString(password) &&
              (password.length > 7) &&
              (password.length < 32));
    };

    Validator.prototype.isTextCode = function(textCode){
      return !!(angular.isDefined(textCode) &&
        angular.isString(textCode));
    };

    Validator.prototype.isName = function(name){
      return !!(angular.isDefined(name) &&
        angular.isString(name));
    };

    Validator.prototype.forKey = function(key,value){
      if(angular.isDefined(key) &&
         angular.isString(key)){
        switch(key){
          case 'name':
            return this.isName(value);
            break;
          case 'firstName':
            return this.isName(value);
            break;
          case 'lastName':
            return this.isName(value);
            break;
          case 'password':
            return this.isPassword(value);
            break;
          case 'textCode':
            return this.isTextCode(value);
            break;
          case 'email':
            return this.isEmail(value);
            break;
          default:
            return angular.isDefined(value);
            break;
        }
      }
      else return false;
    };

    return Validator
  });
