'use strict';

angular.module('omnipledge.services')
  .factory('Donation', function(API, $http, $log, $q ,Account, Validator, $rootScope, $filter) {

    var validator = new Validator();

    var Donation = function(params) {
      if(angular.isDefined(params)){
        this.id = params['id'];
        this.donorId = params['donorId'];
        this.firstName = params['firstName'];
        this.lastName = params['lastName'];
        this.name = params['name'];
        this.phone = params['phone'];
        this.email = params['email'];
        this.amount = params['amount'];
        this.stripeToken = params['stripeToken'];
        this.type = params['type'];
        this.timestamp = params['timestamp'];
        this.method = params['method'];
      }

      this.donate = function(number, cvc, expmonth, expyear){
        var deferred = $q.defer();
        console.log('donating');
        var self = this;
        if(
          angular.isDefined(self.id) &&
          angular.isDefined(cvc) &&
          angular.isDefined(number) &&
          angular.isDefined(expmonth) &&
          angular.isDefined(expyear)){
          console.log('valid donation');

          Stripe.card.createToken({
            number: number,
            cvc: cvc,
            exp_month: expmonth,
            exp_year: expyear
          }, function(status, response) {
            if (response.error) {
              deferred.reject(response.error.message);
            } else {

              if(!response['id']){
                deferred.reject('Our payment processor doesn\'t seem to be online. Don\'t worry, your card hasn\'t been charged. Please refresh and try again.');
              }
              else{
                var payload = {
                  name:self.name,
                  phone:self.phone,
                  email:self.email,
                  amount:self.amount,
                  stripeCardToken:response['id']
                };

                $http.post($rootScope.postUrl, payload).then(function(){
                  deferred.resolve();
                }, function(error){
                  deferred.reject(error['userMessage']);
                });
              }
            }
          });
        }
        else{
          deferred.reject('There\'s been a mistake. Don\'t worry, your card hasn\'t been charged. Please refresh and try again.');
        }

        return deferred.promise;
      };
    };

    Donation.get = function(id){
      var deferred = $q.defer();

      $http.get(API.DONATION + '/' + id).then(function(response) {
        deferred.resolve(new Donation(response));
      },function(response){
        deferred.reject(response['userMessage']);
      });

      return deferred.promise;
    };

    Donation.all = function(){
      var deferred = $q.defer();

      var url = API.DONATION+Account.authParams();

      $http.get(url).then(function(response) {
        if(response['data'] == 'null') {
          deferred.resolve(null);
        }else{
          deferred.resolve($filter('orderBy')(response['data'], 'Timestamp', true));
        }
      },function(response){
        deferred.reject(response['userMessage']);
      });

      return deferred.promise;
    };

    Donation.prototype.update = function(key,value){
      var deferred = $q.defer();

      if(validator.forKey(key, value) && angular.isDefined(this.id)){
        var self = this;    //Scopes this so it can be used in the callback below
        var payload = {key:value};
        this[name] = value; //Update the property locally

        $http.put(API.DONATION + '/' + this.id, payload).then(function() {
          self[name] = value;
          deferred.resolve();
        },function(response){
          deferred.reject(response['userMessage']);
        });
      }
      else if(!angular.isDefined(this.id)){
        deferred.reject('Cannot update an object that does not yet have an id');
      }
      else{
        deferred.reject('Field not valid');
      }

      return deferred.promise;
    };

    Donation.updateDonorPaymentCardInfo = function(id, number, cvc, exp_month, exp_year) {
      var deferred = $q.defer();

      function stripeResponseHandler(status, response) {
        if (response.error) {
          $log.error('omnipledge.services:Subscription+updateDonorPaymentCardInfo | Stripe error getting payment card token');
          $log.error('omnipledge.services:Subscription+updateDonorPaymentCardInfo | Error message ' + angular.toJson(response['error']));
          deferred.reject();
        } else {
          $log.debug('omnipledge.services:Subscription+updateDonorPaymentCardInfo | Payment card token acquired');
          $log.debug('omnipledge.services:Subscription+updateDonorPaymentCardInfo | Sending to back end');

          var url = API.DONOR_UPDATE + Account.authParams();
          var request_def = {
            "method": 'POST',
            url: url,
            data: {
              "donorID": id,
              "token": response['id'],
            }
          };

          $http(request_def).
            success(function() {
              $log.info('omnipledge.services:Subscription+updateDonorPaymentCardInfo | Successful saving payment card token');
              deferred.resolve();
            }).
            error(function(data) {
              $log.error('omnipledge.services:Subscription+updateDonorPaymentCardInfo | Error saving payment card token');
              $log.error('omnipledge.services:Subscription+updateDonorPaymentCardInfo | Error message ' + data['userMessage']);
              deferred.reject(data['userMessage']);
            });
        }
      }

      Stripe.card.createToken({
        number: number,
        cvc: cvc,
        exp_month: exp_month,
        exp_year: exp_year
      }, stripeResponseHandler);

      return deferred.promise;
    }


    return Donation;
  });
