'use strict';

angular.module('omnipledge.services')
  .factory('Bank', function($http, $log, Account, $q, API) {
    var Bank = function(){};

    Bank.addAccount = function(params){
      var deferred = $q.defer();

      function stripeResponseHandler(status, response){
        if (response.error){
          $log.error('omnipledge.services:Bank+addAccount | Stripe error adding bank account');
          $log.error('omnipledge.services:Bank+addAccount | Error message '+angular.toJson(response['error']));
          deferred.reject(response['error']['message']);
        }
        else{
          $log.debug('omnipledge.services:Bank+addAccount | Adding bank account');
          var url = API.ACCOUNTS_ADD_BANK+Account.authParams();
          var request_def = {method: 'POST', url: url, data:{
            "name":params.legalName,
            "corporation":params.corporation,
            "bank":true,
            "token":response['id']
          }};

          $http(request_def).
            success(function() {
              $log.info('omnipledge.services:Bank+addAccount | Successful bank account add');
              deferred.resolve();
            }).
            error(function(data) {
              $log.error('omnipledge.services:Bank+addAccount | Error adding bank account');
              $log.error('omnipledge.services:Bank+addAccount | Error message '+data['userMessage']);
              deferred.reject(data['userMessage']);
            });
        }
      }

      Stripe.bankAccount.createToken({
        country: params.countryCode,
        routingNumber: params.routingNumber,
        accountNumber: params.accountNumber
      }, stripeResponseHandler);

      return deferred.promise;
    };

    return Bank;
  });
