'use strict';

angular.module('omnipledge.services')
  .service('API', function API() {
    var api = {};

    //PRIVATE
    api._TEST_URL = "https://omnipledge-staging.appspot.com";
    // api._PROD_URL = "https://omnipledge-production.appspot.com";
    api._PROD_URL = "http://localhost:8080"
    api._CURRENT_VERSION = "/api/v2";

    //Model routes
    api._ACCOUNTS = '/account';
    api._DONATION = '/donation';
    api._SUBSCRIPTION = '/subscription';

    //Set root URL
    api._ROOT_URL = api._PROD_URL + api._CURRENT_VERSION;

    //PUBLIC
    api.ACCOUNTS = api._ROOT_URL + api._ACCOUNTS;
    api.DONATION = api._ROOT_URL + api._DONATION;

    api.ACCOUNTS_LOGIN = api._ROOT_URL + api._ACCOUNTS + '/login';
    api.ACCOUNTS_CHANGE_PASSWORD = api._ROOT_URL + api._ACCOUNTS + '/change-password';
    api.ACCOUNTS_RESET = api._ROOT_URL + api._ACCOUNTS + '/reset';
    api.ACCOUNTS_ADD_BANK = api._ROOT_URL + api._ACCOUNTS + '/bank';

    api.SUBSCRIPTION_SET_PAYMENT_INFO = api._ROOT_URL + api._SUBSCRIPTION + '/set-payment-info';
    api.SUBSCRIPTION_CANCEL = api._ROOT_URL + api._SUBSCRIPTION + '/cancel';

    api.DONOR_UPDATE = api._ROOT_URL + api._DONATION + '/donorupdate';

    return api;
  });
