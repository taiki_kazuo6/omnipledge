'use strict';

angular.module('omnipledge.admin')
  .constant('Intercom', window.Intercom);
