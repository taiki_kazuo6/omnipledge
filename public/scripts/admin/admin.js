'use strict';

angular.module('omnipledge.admin', [
  'ngRoute',
  'omnipledge.services',
  'ui.bootstrap',
  'ngCsv'
])
  .config(function ($routeProvider, $httpProvider) {

    var interceptor = ['$rootScope', '$q', '$location', 'GateKeeper', function (scope, $q, $location, GateKeeper) {
      function success(response) {
        return response;
      }
      function error(response) {
        var status = response.status;
        if (status === 401) {
          GateKeeper.lock();
          $location.path('/app/login');
          return;
        }
        // otherwise
        return $q.reject(response);
      }
      return function (promise) {
        return promise.then(success, error);
      };
    }];

    $httpProvider.responseInterceptors.push(interceptor);

    $routeProvider
      .when('/app/login', {
        templateUrl: 'views/admin/login.html',
        controller: 'LoginCtrl'
      })
      .when('/app/login/:param', {
        templateUrl: 'views/admin/login.html',
        controller: 'LoginCtrl'
      })
      .when('/app/help', {
        templateUrl: 'views/admin/help.html',
        controller: 'HelpCtrl'
      })
      .when('/app/dashboard', {
        templateUrl: 'views/admin/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .when('/app/account', {
        templateUrl: 'views/admin/account.html',
        controller: 'AccountCtrl'
      })
      .when('/app/subscription', {
        templateUrl: 'views/admin/subscription.html',
        controller: 'SubscriptionCtrl'
      })
      .otherwise({
        redirectTo: '/app/login'
      });
  });
