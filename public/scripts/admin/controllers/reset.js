'use strict';

angular.module('omnipledge.admin')
  .controller('ResetCtrl', function ($scope, Account, $modal, $log, $location, $modalInstance) {

    $scope.error = null;
    $scope.request = {
      oldPassword:"",
      newPassword:""
    };

    $scope.ok = function () {
      Account.changePassword(request['oldPassword'], request['newPassword']).then(function(){
        $scope.error = null;
        $modalInstance.close();
      }, function(errorMessage){
        $scope.error = errorMessage;
      });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  });
