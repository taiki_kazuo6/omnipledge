'use strict';

angular.module('omnipledge.admin')
  .controller('AccountCtrl', function ($scope, Account, Bank, $modal, $log, $location) {

    $scope.email = Account.get('email');
    $scope.textCode = Account.get('textCode');
    $scope.name = Account.get('name');
    $scope.hasBankAccount = Account.get('bankAccount');
    $scope.subscriptionExists = (Account.get('planSubscriptionId') != "");

    $scope.signout = function(){
      Account.logout();
      $location.path('/app/login');
    };

    $scope.linkBank = function () {
      var modalInstance = $modal.open({
        templateUrl: 'views/admin/modals/link-bank-account.html',
        controller: 'BankCtrl',
        size:'lg'
      });

      modalInstance.result.then(function () {
        $log.info('Modal success at: ' + new Date());
        Account.addedBankAccount();
        $scope.hasBankAccount = Account.get('bankAccount');
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.resetPassword = function () {
      var modalInstance = $modal.open({
        templateUrl: 'views/admin/modals/change-password.html',
        controller: 'ResetCtrl',
        size:'sm'
      });

      modalInstance.result.then(function () {
        $log.info('Modal success at: ' + new Date());
      }, function () {
        $log.warn('Modal dismissed at: ' + new Date());
      });
    };
  });
