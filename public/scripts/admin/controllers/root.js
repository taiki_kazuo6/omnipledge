'use strict';

angular.module('omnipledge.admin')
  .controller('RootCtrl', function (Account, Intercom, $log, GateKeeper, $rootScope, $location) {

    GateKeeper.watch($rootScope);
    GateKeeper.allow(['/app/login']);
    GateKeeper.onUnlock(function(){
      Intercom("boot", {
        user_hash: Account.get('userHash'),
        app_id: "e10qpkjd",
        email: Account.get('email'),
        created_at: Account.get('timestamp'),
        name: Account.get('userName'),
        widget: {
          activator: "#IntercomDefaultWidget"
        }
      });

      Intercom('reattach_activator');
      $log.info('omnipledge.admin:RootCtrl+GateKeeper.onUnlock | Booted Intercom.');
    });

    GateKeeper.lock(function(){
      return !Account.get('isAuthenticated');
    }, function(){
      $location.path('/app/login');
    });

  });
