'use strict';

angular.module('omnipledge.admin')
  .controller('LoginCtrl', function ($scope, Account, $routeParams, $location) {

    $scope.result = '';
    $scope.error = false;
    $scope.loading = false;
    $scope.loggedIn = false;
    $scope.signedUp = false;

    $scope.user={
      email:"",
      password:""
    };

    if ($routeParams.param == 'register-success') {
      $scope.signedUp = true;
    }

    $scope.login = function(user) {
      if (!$scope.signedUp || !$scope.loading) {
        $scope.loading = true;

        Account.login(user.email, user.password).then(function () {
          $scope.error = false;
          $scope.loading = false;
          $scope.loggedIn = true;
          $location.path('/app/dashboard');
        }, function (error) {
          $scope.loading = false;
          $scope.error = true;
          $scope.result = error;
        });
      }
    }

  });