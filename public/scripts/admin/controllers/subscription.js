'use strict';

angular.module('omnipledge.admin')
  .controller('SubscriptionCtrl', function ($scope, $rootScope, Account, Subscription, $location) {

    $scope.submitting = false;
    $scope.success = false;
    $scope.successCancel = false;
    $scope.error = false;
    $scope.errorText = '';
    $scope.subscriptionExists = (Account.get('planSubscriptionId') != "");

    function showError(text) {
      $scope.success = false;
      $scope.successCancel = false;
      $scope.error = true;
      $scope.errorText = text;
    }

    $scope.submitPaymentForm = function(){
      $scope.submitting = true;
      Subscription.setPaymentCardInfo({
        formId: 'subscription-payment-form',
      })
      .then(function(res) {
        $scope.submitting = false;
        $scope.success = true;
        $scope.successCancel = false;
        $scope.error = false;
        Account.set('planSubscriptionId', res.planSubscriptionId);
        window.location.reload();
      })
      .catch(function(res) {
        $scope.submitting = false;
        showError(res);
      });
    }

    $scope.cancelSubscription = function() {
      if (!confirm('Are you sure to cancel your subscription?')) {
        return;
      }
      $scope.canceling = true;
      Subscription.cancel()
      .then(function() {
        $scope.canceling = false;
        $scope.successCancel = true;
        $scope.success = false;
        $scope.error = false;
        Account.set('planSubscriptionId', '');
        window.location.reload();
      })
      .catch(function(res) {
        $scope.canceling = false;
        showError(res);
      })
    }

  });