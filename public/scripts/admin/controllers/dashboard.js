'use strict';

angular.module('omnipledge.admin')
  .controller('DashboardCtrl', function ($scope, Account, Donation, $location) {

    $scope.donations = [];
    $scope.textCode = Account.get('textCode');
    $scope.hasBankAccount = Account.get('bankAccount');
    $scope.subscriptionExists = (Account.get('planSubscriptionId') != "");

    Donation.all().then(function(donations){
      console.log('donations!');
      $scope.donations = donations;
    }, function(error){
      console.log('error!');
      console.log(error);
    });

    $scope.signout = function(){
      Account.logout();
      $location.path('/app/login');
    };

    $scope.total = function(){
      var tot = 0;
      angular.forEach($scope.donations, function(donation, index){
        tot += donation['Amount'];
      });
      return tot;
    }
  });