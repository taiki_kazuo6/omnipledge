'use strict';

angular.module('omnipledge.admin')
  .controller('BankCtrl', function ($scope, Account, Bank, $modal, $log, $modalInstance) {

    $scope.bankAccount = {
      "corporation":false,
      "legalName":"",
      "accountNumber":"",
      "routingNumber":"",
      "countryCode":"US"
    };

    $scope.error = null;

    $scope.ok = function () {
      Bank.addAccount($scope.bankAccount).then(function(){
        $scope.error = null;
        $modalInstance.close();
      },function(errorMessage){
        $scope.error = errorMessage;
        $log.error(errorMessage);
      });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  });