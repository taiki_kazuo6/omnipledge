'use strict';

angular.module('omnipledge.donate', [
  'ngRoute',
  'ngCookies',
  'omnipledge.services',
  'angularPayments'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/amount', {
        templateUrl: 'views/donate/select-amount.html',
        controller: 'AmountCtrl'
      })
      .when('/info', {
        templateUrl: 'views/donate/personal-info.html',
        controller: 'InfoCtrl'
      })
      .when('/payment', {
        templateUrl: 'views/donate/payment-info.html',
        controller: 'PaymentCtrl'
      })
      .when('/donorupdate', {
        templateUrl: 'views/donate/payment-update.html',
        controller: 'DonorUpdateCtrl'
      })
      .otherwise({
        redirectTo: '/amount'
      });
  });
