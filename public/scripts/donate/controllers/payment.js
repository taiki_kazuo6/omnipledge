'use strict';

angular.module('omnipledge.donate')
  .controller('PaymentCtrl', function ($scope, $rootScope, Donation) {
    $scope.organization = $rootScope.org.name;
    $scope.donation = $rootScope.donation;


    $scope.donate = function(){

      if(!$scope.disabled && $scope.expiry){
        $scope.loading = true;

        $('#process').removeClass('hidden');
        $('#process').addClass('animated flipInX');
        var donation = new Donation($scope.donation);
        var expiry = $scope.expiry.split("/");
        donation.donate($scope.number, $scope.cvc, expiry[0], expiry[1]).then(function(){
          $scope.error = false;
          $scope.disabled = true;
          $scope.loading = false;
          $scope.message = "Thanks!";
          $scope.disabled = true;
          $('#thanks').addClass('animated bounceInUp');
          $('#process').removeClass('flipInX');
          $('#process').addClass('flipOutX');
          $('#process').removeClass('animated');

        }, function(message){
          $scope.loading = false;
          $scope.error = true;
          $scope.message = message;
          $('#process').removeClass('flipInX');
          $('#process').addClass('flipOutX');
          $('#process').removeClass('animated');

        });
      }
      else{
        $('#donate-container').addClass('shake animated');
      }

    };
    $('#donate-container').on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $('#donate-container').removeClass('shake animated');
    });

      $('#process').on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        if($scope.loading){
        }
        else{
          $('#process').removeClass('animated');
          $('#process').addClass('hidden');
        }
      });

  });