'use strict';

angular.module('omnipledge.donate')
  .controller('DonorUpdateCtrl', function ($scope, $rootScope, $location, Donation) {

    $scope.donorID = $rootScope.donorID;
    $scope.error = false;
    $scope.disabled = false;
    $scope.number = '';
    $scope.cvc = '';
    $scope.expiry = '';

    function loadingBegin() {
      $('#process').removeClass('hidden');
      $('#process').addClass('animated flipInX');
    }

    function loadingEnd() {
      $('#process').addClass('hidden');
      $('#process').removeClass('flipInX');
      $('#process').addClass('flipOutX');
      $('#process').removeClass('animated');
    }

    $scope.submitForm = function(e) {
      loadingBegin();
      var expiry = $scope.expiry.split("/");
      Donation.updateDonorPaymentCardInfo(
        $scope.donorID,
        $scope.number,
        $scope.cvc,
        expiry[0],
        expiry[1]
      )
      .then(function() {
        loadingEnd();
        $scope.error = false;
      })
      .catch(function(res) {
        loadingEnd();
        $scope.error = true;
        $scope.message = res;
      });
    }

  });
