'use strict';

angular.module('omnipledge.donate')
  .controller('AmountCtrl', function ($scope, $rootScope, $location) {
    $scope.organization = $rootScope.org.name;
    $scope.amount = $rootScope.donation.amount/100;

    $scope.setAmount = function(number){
      $scope.other = false;
      $scope.amount = number*1;
      console.log($scope.amount);
      $scope.next();
    };

    $scope.isValid = function(){
      console.log(!(angular.isNumber($scope.amount) && $scope.amount > 0));
      console.log($scope.amount);

      return (angular.isNumber($scope.amount) && $scope.amount > 0);
    };

    $scope.showOther = function(){
      $scope.other = true;
    };

    $scope.next = function(){
      $rootScope.donation.amount = $scope.amount*100;
      $location.path('info');
    };

  });