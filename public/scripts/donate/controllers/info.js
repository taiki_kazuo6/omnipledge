'use strict';

angular.module('omnipledge.donate')
  .controller('InfoCtrl', function ($scope, $rootScope, $location) {
    $scope.organization = $rootScope.org.name;
    $scope.donation = $rootScope.donation;

    $scope.next = function(){
      $rootScope.donation.name = $scope.donation.name;
      $rootScope.donation.email = $scope.donation.email;
      $rootScope.donation.phone = $scope.donation.phone;

      $location.path('payment');
    }
  });