package omnipledge

import (
	"encoding/json"
	"github.com/martini-contrib/render"
	"net/http"
	//	"strings"
)

const (
	ApiErrorBaseUrl             = "https://github.com/RaisinApp/OmniPledge/wiki/Errors"
	ApiErrorRequestProblem      = "Problem performing request.  Please alert the Account owner if the problem continues.  Error Code: "
	ApiErrorRequestNotSupported = "The attempted request is not supported without a user login.  Please alert the Account owner if the problem continues.  Error Code: "
)

type ApiError struct {
	Status      int    `json:"status"`
	Code        int    `json:"code"`
	Error       string `json:"error"`
	UserMessage string `json:"userMessage"`
	//ErrorInfo   string `json:"errorInfo"`
}

//API Errors
func ReturnErrorInternalServerError(r render.Render) {
	var apiErrorInternalServerError ApiError = ApiError{
		Status:      http.StatusInternalServerError,
		Code:        1,
		Error:       "Internal Server Error",
		UserMessage: ApiErrorRequestProblem + "1",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorInternalServerError.Status, apiErrorInternalServerError)
}

func ReturnErrorParsingBody(r render.Render) {
	var apiErrorBodyParsing ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        2,
		Error:       "The request body could not be parsed.  Check if it was formatted properly.",
		UserMessage: ApiErrorRequestProblem + "2",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorBodyParsing.Status, apiErrorBodyParsing)
}

func ReturnErrorNoRoute(r render.Render) {
	var apiErrorNoRoute ApiError = ApiError{
		Status:      http.StatusNotFound,
		Code:        3,
		Error:       "No route for server, 404 Not Found.",
		UserMessage: "Could not locate the desired resource.",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorNoRoute.Status, apiErrorNoRoute)
}

func ReturnErrorValidation(r render.Render, message string) {
	var valError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        4,
		Error:       "The request's data did not pass validation.  Message: " + message,
		UserMessage: message,
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(valError.Status, valError)
}

//App Engine Errors
func ReturnErrorSavingAccount(r render.Render) {
	var apiErrorSavingAccount ApiError = ApiError{
		Status:      http.StatusInternalServerError,
		Code:        5,
		Error:       "Problem saving account.",
		UserMessage: "There was a problem saving the account.",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorSavingAccount.Status, apiErrorSavingAccount)
}
func ReturnErrorSendingEmail(r render.Render) {
	var apiErrorSendingEmail ApiError = ApiError{
		Status:      http.StatusInternalServerError,
		Code:        6,
		Error:       "Problem sending email from GAE.",
		UserMessage: "There was a problem sending the email.",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorSendingEmail.Status, apiErrorSendingEmail)
}

//Account General Errors
//check
func ReturnErrorMatchingConfirmationCode(r render.Render) {
	var apiErrorMatchingConfirmationCode ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        7,
		Error:       "Invalid confirmation code for this account.",
		UserMessage: "The confirmation code does not match. Please try resending your account confirmation.",
	}
	r.JSON(apiErrorMatchingConfirmationCode.Status, apiErrorMatchingConfirmationCode)
}

func ReturnErrorTextCodeInUse(r render.Render) {
	var apiErrorTextCode ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        8,
		Error:       "Text code is in use",
		UserMessage: "This text code already exists. Please pick another one.",
	}
	r.JSON(apiErrorTextCode.Status, apiErrorTextCode)
}
func ReturnErrorGettingAccountByEmail(r render.Render) {
	var apiErrorGettingAccountByEmail ApiError = ApiError{
		Status:      http.StatusInternalServerError,
		Code:        8,
		Error:       "Problem getting account with email.",
		UserMessage: "No accounts found for this email.",
	}
	r.JSON(apiErrorGettingAccountByEmail.Status, apiErrorGettingAccountByEmail)
}
func ReturnErrorEmailInUse(r render.Render) {
	var apiErrorEmailInUse ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        9,
		Error:       "Email in use for exisiting account",
		UserMessage: "There is an already an account registered to this email.",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorEmailInUse.Status, apiErrorEmailInUse)
}

func ReturnErrorInvalidOranization(r render.Render) {
	var apiErrorInvalidOrganization ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        9,
		Error:       "Invalid organization ID",
		UserMessage: "There was a problem finding the organization",
	}
	r.JSON(apiErrorInvalidOrganization.Status, apiErrorInvalidOrganization)
}

//Account Authentication Errors
func ReturnErrorUnauthorized(r render.Render) {
	var apiErrorAuthUser ApiError = ApiError{
		Status:      http.StatusUnauthorized,
		Code:        10,
		Error:       "Invalid user email or token",
		UserMessage: "The session expired.  Please login again.",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorAuthUser.Status, apiErrorAuthUser)
}

func ReturnErrorNoCredentials(r render.Render) {
	var apiErrorAuthNoCreds ApiError = ApiError{
		Status:      http.StatusUnauthorized,
		Code:        11,
		Error:       "No authentication credentials were provided",
		UserMessage: "The session expired.  Please login again.",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorAuthNoCreds.Status, apiErrorAuthNoCreds)
}

func ReturnErrorLogin(r render.Render) {
	var apiErrorLogin ApiError = ApiError{
		Status:      http.StatusForbidden,
		Code:        12,
		Error:       "Invalid email or password",
		UserMessage: "Login did not have a correct email or password.  Please try again or use the password reset function.",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorLogin.Status, apiErrorLogin)
}

func ReturnErrorPasswordChange(r render.Render) {
	var apiErrorPwordReset ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        13,
		Error:       "The user entered a value for the current password that was incorrect.",
		UserMessage: "The current password was incorrect.",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiErrorPwordReset.Status, apiErrorPwordReset)
}

func ReturnDeleteContactError(r render.Render) {
	var apiDeleteContact ApiError = ApiError{
		Status:      http.StatusInternalServerError,
		Code:        14,
		Error:       "Problem deleting contact",
		UserMessage: "Problem deleting contact",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiDeleteContact.Status, apiDeleteContact)
}

func ReturnInvalidContactError(r render.Render) {
	var apiInvalidContact ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        14,
		Error:       "Cannot return contact for that ID",
		UserMessage: "Cannot return contact for that ID",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiInvalidContact.Status, apiInvalidContact)
}

func ReturnInvalidDonationError(r render.Render) {
	var apiInvalidDonation ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        14,
		Error:       "Cannot return donation for that ID",
		UserMessage: "Cannot return donation for that ID",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiInvalidDonation.Status, apiInvalidDonation)
}

func ReturnErrorAddingBankAccount(r render.Render) {
	var apiAddBankError ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        15,
		Error:       "Problem creating bank account",
		UserMessage: "Problem creating bank account",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiAddBankError.Status, apiAddBankError)
}

func ReturnErrorCreatingSubscription(r render.Render) {
	var apiCreateSubscriptionError ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        16,
		Error:       "Problem creating organization subscription",
		UserMessage: "Problem creating organization subscription",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiCreateSubscriptionError.Status, apiCreateSubscriptionError)
}

func ReturnErrorCancelingSubscription(r render.Render) {
	var apiCancelSubscriptionError ApiError = ApiError{
		Status:      http.StatusBadRequest,
		Code:        17,
		Error:       "Problem canceling organization subscription",
		UserMessage: "Problem canceling organization subscription",
		//ErrorInfo:   ApiErrorBaseUrl,
	}
	r.JSON(apiCancelSubscriptionError.Status, apiCancelSubscriptionError)
}

//Util Methods
func (this ApiError) String() string {
	bytes, err := json.Marshal(this)
	if nil != err {
		panic(err)
	}
	return string(bytes)
}
