# Omnipledge
Simple SMS Donations


#Deploying Locally

##Server
1. In `app.go` set the base URL constant, `OPBaseURL` to `http://localhost:8080/` by uncommenting the correct line.
2. In `app.go` set the Stripe API token constant, `OPStripeAPI` to **testing** by uncommenting the correct line.
3. In `app.go` set the Twilio number constant, `OPTwilioNumber`, to **staging** by uncommenting the correct line.
4. Run `goapp serve`

##Client
[Building the Web Client](https://github.com/RaisinApp/OmniPledge/wiki/Building-the-Web-Client)

### Changing the base API URL
If you prefer not to have to hunt for a single, mission-critical constant in production sized codebase that was written months ago by people who weren't you using technology you may have little more than a passing familiarity with, and you can appreciate documentation as a hallmark of quality software development, here's some steps on how to change the API base:

1. Open `/app/scripts/services/models/api.js` ([On Github](https://github.com/RaisinApp/OmniWeb/blob/master/app/scripts/services/models/api.js)).
2. Set `api._ROOT_URL` to whatever you want. 


###Ensure the Donation Form is Live

1. In `public/donate.html` switch the comment to the static text.
```
angular.module('omnipledge.donate')
.controller('RootCtrl', function ($rootScope, $location) {

// $rootScope.postUrl = "";
// $rootScope.donation = {
//   id:'1234',
//   amount:1200,
//   name:'Samuel McLaughlin',
//   email:'s.mclaughlin.wv@gmail.com',
//   phone:'3043894226',
//   completed:true
// };
// $rootScope.org = {
//   name:'Test Org'
// };

$rootScope.postUrl = "{[{.PostURL}]}";
$rootScope.donation = {
id:'{[{.DonationID}]}',
amount:{[{.DonationAmount}]},
name:'{[{.DonorName}]}',
email:'{[{.DonorEmail}]}',
phone:'{[{.DonorNumber}]}',
completed:'{[{.DonationCompleted}]}'
};
$rootScope.org = {
name:'{[{.OrgName}]}'
};
```
### Changing the Stripe Publishable Key
There are two different files where you need to change the stripe publishable key to testing:
####In donate.html and admin.html
Change
```
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
Stripe.setPublishableKey('pk_live_7ipryBVFT7mU2TBpTYjhgjb3');
</script>
```
To
```
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
Stripe.setPublishableKey('pk_test_LrNShwyobU62ouqGhSkAlBsr');
</script>
```

The staging instance is stored on a different Google account than the production instance. Staging instance is stored on my personal account, but it could easily be deployed on any App Engine instance.

# Deploying to Staging

##Server
1. In `app.go` set the email constant, `OPEmailAddress` to `accounts@omnipledge-staging.appspotmail.com` by uncommenting the correct line.
2. In `app.go` set the base URL, `OPBaseURL` to `https://omnipledge-staging.appspot.com/` by uncommenting the correct line. Note: if you're deploying to a different app engine staging server, the URL will be different.
2. In `app.go` set the Stripe API token constant, `OPStripeAPI` to **testing** by uncommenting the correct line.
3. In `app.go` set the Twilio number constant, `OPTwilioNumber`, to **staging** by uncommenting the correct line.
4. In `app.yaml` change the application ID to `omnipledge-staging`

##Client
[Building the Web Client](https://github.com/RaisinApp/OmniPledge/wiki/Building-the-Web-Client)

### Changing the base API URL
If you prefer not to have to hunt for a single, mission-critical constant in production sized codebase that was written months ago by people who weren't you using technology you may have little more than a passing familiarity with, and you can appreciate documentation as a hallmark of quality software development, here's some steps on how to change the API base:

1. Open `/app/scripts/services/models/api.js` ([On Github](https://github.com/RaisinApp/OmniWeb/blob/master/app/scripts/services/models/api.js)).
2. Set `api._ROOT_URL` to whatever you want. 


###Ensure the Donation Form is Live

1. In `public/donate.html` switch the comment to the static text.
```
angular.module('omnipledge.donate')
.controller('RootCtrl', function ($rootScope, $location) {

// $rootScope.postUrl = "";
// $rootScope.donation = {
//   id:'1234',
//   amount:1200,
//   name:'Samuel McLaughlin',
//   email:'s.mclaughlin.wv@gmail.com',
//   phone:'3043894226',
//   completed:true
// };
// $rootScope.org = {
//   name:'Test Org'
// };

$rootScope.postUrl = "{[{.PostURL}]}";
$rootScope.donation = {
id:'{[{.DonationID}]}',
amount:{[{.DonationAmount}]},
name:'{[{.DonorName}]}',
email:'{[{.DonorEmail}]}',
phone:'{[{.DonorNumber}]}',
completed:'{[{.DonationCompleted}]}'
};
$rootScope.org = {
name:'{[{.OrgName}]}'
};
```
### Changing the Stripe Publishable Key
There are two different files where you need to change the stripe publishable key to testing:
####In donate.html and admin.html
Change
```
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
Stripe.setPublishableKey('pk_live_7ipryBVFT7mU2TBpTYjhgjb3');
</script>
```
To
```
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
Stripe.setPublishableKey('pk_test_LrNShwyobU62ouqGhSkAlBsr');
</script>
```



## App Engine Deployment
* Use `appcfg.py --oauth2 update .` if you're Google account uses 2-step authentication
* Use `goapp deploy` otherwise
