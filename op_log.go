package omnipledge

import (
	"appengine"
	"fmt"
)

func LogDebug(c appengine.Context, message string) {
	c.Debugf(message)
}

func LogInfo(c appengine.Context, message string) {
	c.Infof(message)
}

func LogError(c appengine.Context, message string, err error) {
	c.Errorf(fmt.Sprintf("%s\nError: %s", message, err))
}
