package omnipledge_test

import (
	"appengine"
	"appengine/aetest"
	"appengine/memcache"
	// "code.google.com/p/go.crypto/bcrypt"
	"golang.org/x/crypto/bcrypt"
	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/r1cky1337/omnipledge-subscription"
	"math/rand"
	"strconv"
	"time"
)

var _ = Describe("Organization", func() {

	var (
		newOrg   *Organization
		validP   string
		changeP  string
		invalidP string
		noID     string
		orgEmail string
	)
	Context("Testing Organization", func() {
		validP = "ricky123"
		changeP = "ricky 345"
		invalidP = "ricky"
		noID = ""
		orgEmail = "e@mail.com"
		newOrg = &Organization{
			Name:     "MyOrg",
			Email:    orgEmail,
			TextCode: "test",
			Verified: true,
		}

		XIt("Should test calculating the donation total for this pay period", func() {
			//DonationTotalThisPayPeriod
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			// _ = newOrg.Save(context)

			newOrg := &Organization{}
			signupReq := SignupRequest{
				FirstName:       "Ricky",
				LastName:        "Kirkendall",
				DobDay:          11,
				DobMonth:        6,
				DobYear:         1992,
				IsBusiness:      false,
				Email:           orgEmail,
				SSNLastFour:     "1234",
				TOSTimeAccepted: time.Now().Unix(),
				TOSIPAccepted:   "127.0.0.1",
				AddressLineOne:  "1 Mainstreet Ln",
				AddressCity:     "Morgantown",
				AddressState:    "WV",
				AddressZip:      "26505",
			}
			acct, _ := newOrg.CreateManagedAccount(context, signupReq)
			newOrg.SetAccountMangedAccountFields(context, acct)
			cardToken := newOrg.GenerateTestCardToken(context)
			_, _ = newOrg.SubscribeWithPayment(context, cardToken)

		})

		It("Should test getting an email case sensitive", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			_ = newOrg.Save(context)

			testOrg := &Organization{}
			getEr := testOrg.GetByEmail(context, "E@mail.com")
			Ω(getEr).ShouldNot(HaveOccurred())
			Expect(testOrg.TextCode).To(Equal("test"))
		})

		It("Should subscribe an organization to the OP $12 plan", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()

			newOrg := &Organization{}
			signupReq := SignupRequest{
				FirstName:       "Ricky",
				LastName:        "Kirkendall",
				DobDay:          11,
				DobMonth:        6,
				DobYear:         1992,
				IsBusiness:      false,
				Email:           orgEmail,
				SSNLastFour:     "1234",
				TOSTimeAccepted: time.Now().Unix(),
				TOSIPAccepted:   "127.0.0.1",
				AddressLineOne:  "1 Mainstreet Ln",
				AddressCity:     "Morgantown",
				AddressState:    "WV",
				AddressZip:      "26505",
			}
			acct, _ := newOrg.CreateManagedAccount(context, signupReq)
			newOrg.SetAccountMangedAccountFields(context, acct)
			cardToken := newOrg.GenerateTestCardToken(context)
			_, er := newOrg.SubscribeWithPayment(context, cardToken)
			Ω(er).ShouldNot(HaveOccurred())
		})

		It("Should setup a tranfer schedule for a connected account", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()

			newOrg := &Organization{}
			signupReq := SignupRequest{
				FirstName:       "Ricky",
				LastName:        "Kirkendall",
				DobDay:          11,
				DobMonth:        6,
				DobYear:         1992,
				IsBusiness:      false,
				Email:           orgEmail,
				SSNLastFour:     "1234",
				TOSTimeAccepted: time.Now().Unix(),
				TOSIPAccepted:   "127.0.0.1",
				AddressLineOne:  "1 Mainstreet Ln",
				AddressCity:     "Morgantown",
				AddressState:    "WV",
				AddressZip:      "26505",
			}
			acct, _ := newOrg.CreateManagedAccount(context, signupReq)
			newOrg.SetAccountMangedAccountFields(context, acct)
			bankToken := newOrg.GenerateTestBankToken(context)
			_, _ = newOrg.AddBankAccount(context, bankToken)
			acct, err := newOrg.AddTranferSchedule(context)
			Ω(acct.TransferSchedule).ShouldNot(Equal(nil))
			Ω(err).ShouldNot(HaveOccurred())
			Ω(acct.TransferSchedule.WeekAnchor).Should(Equal("friday"))
		})

		It("Should add a bank account to a managed account", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()

			newOrg := &Organization{}
			signupReq := SignupRequest{
				FirstName:       "Ricky",
				LastName:        "Kirkendall",
				DobDay:          11,
				DobMonth:        6,
				DobYear:         1992,
				IsBusiness:      false,
				Email:           orgEmail,
				SSNLastFour:     "1234",
				TOSTimeAccepted: time.Now().Unix(),
				TOSIPAccepted:   "127.0.0.1",
				AddressLineOne:  "1 Mainstreet Ln",
				AddressCity:     "Morgantown",
				AddressState:    "WV",
				AddressZip:      "26505",
			}
			acct, _ := newOrg.CreateManagedAccount(context, signupReq)
			newOrg.SetAccountMangedAccountFields(context, acct)
			bankToken := newOrg.GenerateTestBankToken(context)
			_, er := newOrg.AddBankAccount(context, bankToken)
			Ω(er).ShouldNot(HaveOccurred())
		})

		It("Should set the managed account fields", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()

			newOrg := &Organization{}
			signupReq := SignupRequest{
				FirstName:       "Ricky",
				LastName:        "Kirkendall",
				DobDay:          11,
				DobMonth:        6,
				DobYear:         1992,
				IsBusiness:      false,
				Email:           orgEmail,
				SSNLastFour:     "1234",
				TOSTimeAccepted: time.Now().Unix(),
				TOSIPAccepted:   "127.0.0.1",
				AddressLineOne:  "1 Mainstreet Ln",
				AddressCity:     "Morgantown",
				AddressState:    "WV",
				AddressZip:      "26505",
			}
			acct, e := newOrg.CreateManagedAccount(context, signupReq)
			Ω(e).ShouldNot(HaveOccurred())
			fmt.Println("%v", acct)
			newOrg.SetAccountMangedAccountFields(context, acct)
			fmt.Println("ID ", newOrg.ManagedAccountId)
			fmt.Println("Pub ", newOrg.ManagedAcconutPublishableKey)
			fmt.Println("Secret ", newOrg.ManagedAccountPrivateKey)
			fmt.Println("Fields Needed ", newOrg.VerificationFieldsNeeded)
			Expect(newOrg.ManagedAccountId).ToNot(Equal(""))
			Expect(newOrg.ManagedAcconutPublishableKey).ToNot(Equal(""))
			Expect(newOrg.ManagedAccountPrivateKey).ToNot(Equal(""))
		})

		It("Should create a new organization with a managed stripe account", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()

			newOrg := &Organization{}
			signupReq := SignupRequest{
				FirstName:       "Ricky",
				LastName:        "Kirkendall",
				DobDay:          11,
				DobMonth:        6,
				DobYear:         1992,
				IsBusiness:      false,
				Email:           orgEmail,
				SSNLastFour:     "1234",
				TOSTimeAccepted: time.Now().Unix(),
				TOSIPAccepted:   "127.0.0.1",
				AddressLineOne:  "1 Mainstreet Ln",
				AddressCity:     "Morgantown",
				AddressState:    "WV",
				AddressZip:      "26505",
			}
			acct, err := newOrg.CreateManagedAccount(context, signupReq)
			fmt.Println("%v", acct)
			Ω(err).ShouldNot(HaveOccurred())

		})

		It("Should save a new organization", func() {
			context, e := aetest.NewContext(nil)
			Ω(e).ShouldNot(HaveOccurred())
			defer context.Close()
			err := newOrg.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
		})
		It("Should have a populated ID field", func() {
			Expect(newOrg.ID).ToNot(Equal(noID))
		})
		It("Should set a hash for password", func() {
			err := newOrg.NewPassword(validP)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(len(newOrg.Password)).ToNot(Equal(0))
		})
		It("Should Change the password", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			var _ = newOrg.NewPassword(validP)
			err := newOrg.ChangePassword(validP, changeP)
			Ω(err).ShouldNot(HaveOccurred())
			er := bcrypt.CompareHashAndPassword([]byte(newOrg.Password), []byte(changeP))
			Ω(er).ShouldNot(HaveOccurred())
		})
		It("Should NOT Change the password", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			var _ = newOrg.NewPassword(validP)
			err := newOrg.ChangePassword("fish", changeP)
			Ω(err).Should(HaveOccurred())
		})
		It("Should save changes to existing account", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			oldID := newOrg.ID
			err := newOrg.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(newOrg.ID).To(Equal(oldID))
		})

		It("Should generate a confirmation code in namespaced memcache", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			confCode, err := newOrg.GenerateConfirmationCode(context)
			Ω(err).ShouldNot(HaveOccurred())
			//Check memcache
			namespace, err := OrgNamespace(context, newOrg.TextCode)
			item, err := memcache.Get(namespace, "confirmation")
			Ω(err).ShouldNot(HaveOccurred())
			savedCode := string(item.Value)
			Expect(savedCode).To(Equal(confCode))
		})
		It("Should confirm confirmation code", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			confCode, err := newOrg.GenerateConfirmationCode(context)
			valid, err := newOrg.CheckConfirmationCode(context, confCode)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(valid).To(Equal(true))
		})
		It("Should reset password to random string", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			err := newOrg.NewPassword(validP)
			newP, err := newOrg.ResetPassword(context)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(validP).ToNot(Equal(newP))
		})
		It("Should fail to get organization by email", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			testSave := &Organization{
				Email:    "Ricky@syntropy.io",
				Name:     "Ricky",
				TextCode: "ricky",
			}
			err := testSave.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			testFind := &Organization{}
			e := testFind.GetByEmail(context, "Ricky@floco.io")
			Ω(e).ShouldNot(HaveOccurred())
			Expect(testSave.ID).ToNot(Equal(testFind.ID))
		})
		It("Should get organization by email", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			testSave := &Organization{
				Email:    "Ricky@syntropy.io",
				Name:     "Ricky",
				TextCode: "ricky",
			}
			err := testSave.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			testFind := &Organization{}
			e := testFind.GetByEmail(context, "Ricky@syntropy.io")
			Ω(e).ShouldNot(HaveOccurred())
			Expect(testSave.ID).To(Equal(testFind.ID))
		})
		It("Should get organization by ID", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			testSave := &Organization{
				Email:    "Ricky@syntropy.io",
				Name:     "Ricky",
				TextCode: "ricky",
			}
			err := testSave.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			testFind := &Organization{}
			e := testFind.GetByID(context, testSave.ID)
			Ω(e).ShouldNot(HaveOccurred())
			Expect(testSave.Email).To(Equal(testFind.Email))
		})
		It("Should get organization by textCode", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			testSave := &Organization{
				Email:    "Ricky@syntropy.io",
				Name:     "Ricky",
				TextCode: "ricky",
			}
			err := testSave.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			testFind := &Organization{}
			e := testFind.GetByTextCode(context, "ricky")
			Ω(e).ShouldNot(HaveOccurred())
			Expect(testSave.Email).To(Equal(testFind.Email))
		})
		It("Should fail to get organization by textCode", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			testSave := &Organization{
				Email:    "Ricky@syntropy.io",
				Name:     "Ricky",
				TextCode: "ricky",
			}
			err := testSave.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			testFind := &Organization{}
			e := testFind.GetByTextCode(context, "fish")
			Ω(e).Should(HaveOccurred())
			Expect(testSave.Email).ToNot(Equal(testFind.Email))
		})

		It("Should save auth token to memcache", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			e := newOrg.Save(context)
			Ω(e).ShouldNot(HaveOccurred())
			err := newOrg.SaveAuthTokenToMemCache(context, "hello")
			Ω(err).ShouldNot(HaveOccurred())
			//Check memcache for value
			nameContext, er := appengine.Namespace(context, newOrg.TextCode)
			Ω(er).ShouldNot(HaveOccurred())
			item, err := memcache.Get(nameContext, "hello")
			Ω(err).ShouldNot(HaveOccurred())
			value := string(item.Value)
			Expect(value).To(Equal("true"))
		})
		It("Should login", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			err := newOrg.NewPassword(validP)
			Ω(err).ShouldNot(HaveOccurred())
			e := newOrg.Save(context)
			Ω(e).ShouldNot(HaveOccurred())
			token, er := newOrg.Login(context, newOrg.Email, validP)
			Ω(er).ShouldNot(HaveOccurred())
			Expect(len(token)).ToNot(Equal(0))
		})
		It("Should fail login ", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			err := newOrg.NewPassword(validP)
			Ω(err).ShouldNot(HaveOccurred())
			e := newOrg.Save(context)
			Ω(e).ShouldNot(HaveOccurred())
			token, er := newOrg.Login(context, newOrg.Email, "fish")
			Ω(er).Should(HaveOccurred())
			Expect(len(token)).To(Equal(0))
		})
		It("Should authenticate a login token", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			e := newOrg.NewPassword(validP)
			err := newOrg.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			Ω(e).ShouldNot(HaveOccurred())
			//Login
			token, er := newOrg.Login(context, newOrg.Email, validP)
			Ω(er).ShouldNot(HaveOccurred())
			//Check token
			errr := newOrg.Authenticate(context, newOrg.Email, token)
			Ω(errr).ShouldNot(HaveOccurred())
		})
		It("Should save a puzzle solution and get it when called", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			_ = newOrg.Save(context)
			rand.Seed(time.Now().UnixNano())
			rand1 := RandomDigit()
			rand2 := RandomDigit()
			sum := rand1 + rand2
			fmt.Printf("Rand 1: %d  Rand 2: %d", rand1, rand2)
			sumString := strconv.FormatInt(sum, 10)
			from := "+13046103672"
			err := newOrg.SaveToMemCache(context, from, sumString)
			Ω(err).ShouldNot(HaveOccurred())

			//Get solution
			solution, e := newOrg.GetPuzzleAnswerForNumber(context, from)
			Ω(e).ShouldNot(HaveOccurred())
			expectedSolutionNum := rand1 + rand2
			Expect(solution).To(Equal(expectedSolutionNum))
		})
		It("Should get all donations for an organization", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			donor := &Donor{}
			donor.Name = "ricky kirkendall"
			er := donor.Save(context)
			Ω(er).ShouldNot(HaveOccurred())
			e := newOrg.Save(context)
			Ω(e).ShouldNot(HaveOccurred())
			donation, erNewDon1 := donor.NewDonation(context, newOrg)
			Ω(erNewDon1).ShouldNot(HaveOccurred())
			donation.Amount = 500
			donation.Completed = true
			erDon1Save := donation.Save(context)
			Ω(erDon1Save).ShouldNot(HaveOccurred())
			donation2, erNewDon2 := donor.NewDonation(context, newOrg)
			Ω(erNewDon2).ShouldNot(HaveOccurred())
			donation2.Amount = 700
			donation2.Completed = true
			erDon2Save := donation2.Save(context)
			Ω(erDon2Save).ShouldNot(HaveOccurred())

			Expect(donation.ID).ToNot(Equal(""))
			Expect(donation2.ID).ToNot(Equal(""))

			donations, err := newOrg.GetAllDonations(context)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(len(donations)).To(Equal(2))
			fmt.Printf("%+v\n\n", donations[0])
			fmt.Printf("%+v", donations[1])
		})

		It("Should get all donors for an organization", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			donor1 := &Donor{}
			donor1.Name = "ricky kirkendall"
			erD1 := donor1.Save(context)
			Ω(erD1).ShouldNot(HaveOccurred())
			donor2 := Donor{}
			donor2.Name = "bobby donorson"
			erD2 := donor2.Save(context)
			Ω(erD2).ShouldNot(HaveOccurred())
			e := newOrg.Save(context)
			Ω(e).ShouldNot(HaveOccurred())
			donation, erNewDon1 := donor1.NewDonation(context, newOrg)
			Ω(erNewDon1).ShouldNot(HaveOccurred())
			donation.Amount = 500
			donation.Completed = true
			erDon1Save := donation.Save(context)
			Ω(erDon1Save).ShouldNot(HaveOccurred())
			donation2, erNewDon2 := donor2.NewDonation(context, newOrg)
			Ω(erNewDon2).ShouldNot(HaveOccurred())
			donation2.Amount = 700
			donation2.Completed = true
			erDon2Save := donation2.Save(context)
			Ω(erDon2Save).ShouldNot(HaveOccurred())
			donors, err := newOrg.GetAllDonors(context)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(len(donors)).To(Equal(2))
		})

		It("Should generate a test bank account token", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			_ = newOrg.Save(context)
			token := GenerateTestStripeBankAccountToken(context)
			fmt.Println(token)
			Expect(token).ToNot(Equal(""))
		})
		// It("Should add a bank account for organization", func() {
		// 	context, _ := aetest.NewContext(nil)
		// 	defer context.Close()

		// 	_ = newOrg.Save(context)
		// 	token := GenerateTestStripeBankAccountToken(context)
		// 	err := newOrg.AddBankAccount(context, true, true, token)
		// 	Ω(err).ShouldNot(HaveOccurred())
		// 	Expect(newOrg.StripeAccountToken).ToNot(Equal(""))
		// })

	})
})
