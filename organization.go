package omnipledge

import (
	"appengine"
	"appengine/datastore"
	"appengine/memcache"
	// "appengine/urlfetch"
	// "code.google.com/p/go.crypto/bcrypt"
	"golang.org/x/crypto/bcrypt"
	"errors"
	"fmt"
	"net/http"
	"crypto/tls"
	"github.com/dchest/uniuri"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/client"
	"strconv"
	"strings"
	"time"
)

type Organization struct {
	ID           string `datastore:"-"`
	Name         string `json:"name"`
	TextCode     string `json:"textCode"`
	Email        string `json:"email"`
	Password     string
	Verified     bool
	Timestamp    int64
	EIN          string
	Confirmation string

	ContactFirstName string
	ContactLastName  string
	ContactDOBString string
	IsBusiness       bool

	ManagedAccountId							string `json:"managedAccountId"`
	ManagedAccountPrivateKey			string
	ManagedAcconutPublishableKey	string `json:"managedAccountPubKey"`

	TransfersEnabled           		bool     `json:"accountTransfersEnabled"`
	VerificationDueBy          		int64    `json:"verificationDueBy"`
	VerificationFieldsNeeded   		[]string `json:"verificationFieldsNeeded"`
	VerificationDisabledReason 		string   `json:"stripeAccountDisabledReason"`

	SubscriptionCustomerId 				string
	PlanSubscriptionId     				string

	PaymentInformationNeedsUpdated bool
	Deactivated                    bool
	DelinquencyDate                int64

	currentLoginToken							string `json:"token"`
}

const (
	confirmationKey = "confirmation"
)

////// Init

func (this *Organization) PublicJSON() map[string]interface{} {
	hmacKey := GenerateHMACUserID(this.Email)
	return map[string]interface{}{
		"email":                          this.Email,
		"timestamp":                      this.Timestamp,
		"name":                           this.Name,
		"hmac":                           hmacKey,
		"textCode":                       this.TextCode,
		"opVerified":                     this.Verified,
		"transfersEnabled":               this.TransfersEnabled,
		"deactivated":                    this.Deactivated,
		"paymentInformationNeedsUpdated": this.PaymentInformationNeedsUpdated,
		"verificationFieldsNeeded":       this.VerificationFieldsNeeded,
		"planSubscriptionId":             this.PlanSubscriptionId,
		"customerId":                     this.SubscriptionCustomerId,
		"token":													this.currentLoginToken,
	}
}

////// Stripe Functions
func (this *Organization) OrganizationStripeClient(c appengine.Context) *client.API {
	/// use on app engine
	// httpClient := urlfetch.Client(c)

	/// use on local
	tr := &http.Transport{
    TLSClientConfig: &tls.Config{ MinVersion: 1 },
    DisableCompression: true,
	}
	httpClient := &http.Client{Transport: tr}

	stripeClient := client.New(this.ManagedAccountPrivateKey, stripe.NewBackends(httpClient))
	return stripeClient
}

func (this *Organization) CreateManagedAccount(c appengine.Context, data SignupRequest) (*stripe.Account, error) {
	// Create managed account
	stripeClient := StripeOPClient(c)

	legalAdd := stripe.Address{
		Line1:   data.AddressLineOne,
		Line2:   data.AddressLineTwo,
		City:    data.AddressCity,
		State:   data.AddressState,
		Zip:     data.AddressZip,
		Country: data.AddressCountry,
	}

	legalEntity := &stripe.LegalEntity{}
	if data.IsBusiness {
		legalEntity.Type = "business"
	} else {
		legalEntity.Type = "individual"
	}
	legalEntity.First = data.FirstName
	legalEntity.Last = data.LastName
	legalEntity.SSN = data.SSNLastFour
	legalEntity.DOB = stripe.DOB{
		Day:   data.DobDay,
		Month: data.DobMonth,
		Year:  data.DobYear,
	}
	legalEntity.Address = legalAdd

	tosAcc := &stripe.TOSAcceptanceParams{
		Date: data.TOSTimeAccepted,
		IP:   data.TOSIPAccepted,
	}

	managedAcctParams := &stripe.AccountParams{
		Country:       "US",
		Email:         data.Email,
		TOSAcceptance: tosAcc,
		Managed:       true,
		BusinessName:  data.BusinessName,
		LegalEntity:   legalEntity,
	}

	managedAccount, err := stripeClient.Account.New(managedAcctParams)

	return managedAccount, err
}

func (this *Organization) SetAccountMangedAccountFields(c appengine.Context, managedAcct *stripe.Account) {
	this.ManagedAccountId = managedAcct.ID
	this.ManagedAcconutPublishableKey = managedAcct.Keys.Publish
	this.ManagedAccountPrivateKey = managedAcct.Keys.Secret
	this.TransfersEnabled = managedAcct.TransfersEnabled
	this.VerificationDisabledReason = managedAcct.Verification.DisabledReason
	// this.VerificationDueBy = *managedAcct.Verification.Due
	this.VerificationFieldsNeeded = managedAcct.Verification.Fields
}

func (this *Organization) AddTranferSchedule(c appengine.Context) (*stripe.Account, error) {
	orgStripeClient := this.OrganizationStripeClient(c)
	transferSchedule := &stripe.TransferScheduleParams{
		Interval:   "weekly",
		WeekAnchor: "friday",
	}

	return orgStripeClient.Account.Update(
		this.ManagedAccountId,
		&stripe.AccountParams{TransferSchedule: transferSchedule},
	)
}

func (this *Organization) AddBankAccount(c appengine.Context, stripeBankToken string) (*stripe.BankAccount, error) {
	orgStripeClient := this.OrganizationStripeClient(c)
	bankAcctParams := &stripe.BankAccountParams{
		AccountID: this.ManagedAccountId,
		Token:     stripeBankToken,
	}

	transferSchedule := &stripe.TransferScheduleParams{
		Interval:   "weekly",
		WeekAnchor: "friday",
	}

	_, _ = orgStripeClient.Account.Update(
		this.ManagedAccountId,
		&stripe.AccountParams{TransferSchedule: transferSchedule},
	)

	return orgStripeClient.BankAccounts.New(bankAcctParams)
}

func (this *Organization) DonationShouldTakeFee(donationAmount int64, c appengine.Context) bool {
	total, e := this.DonationTotalThisPayPeriod(c)
	if e != nil {
		LogError(c, "Problem calculating donation total for current pay period", e)
	}
	if (total + donationAmount) > OPSubscriptionPeriodLimit {
		return true
	} else {
		return false
	}
}

func (this *Organization) DonationTotalThisPayPeriod(c appengine.Context) (int64, error) {

	opStripeClient := StripeOPClient(c)
	sub, err := opStripeClient.Subs.Get(
		this.PlanSubscriptionId,
		&stripe.SubParams{Customer: this.SubscriptionCustomerId},
	)

	if err != nil {
		return 0, err
	}

	// Get all transfers to organization account with filters
	params := &stripe.TransferListParams{}
	params.Filters.AddFilter("destination", "", this.ManagedAccountId)
	params.Filters.AddFilter("created", "lt", strconv.FormatInt(sub.PeriodEnd, 10))
	params.Filters.AddFilter("created", "gt", strconv.FormatInt(sub.PeriodStart, 10))
	i := opStripeClient.Transfers.List(params)
	total := int64(0)
	for i.Next() {
		c := i.Transfer()
		total += c.Amount
	}
	return total, nil
}

func (this *Organization) SubscribeWithPayment(c appengine.Context, payCardToken string) (*stripe.Customer, error) {
	opStripeClient := StripeOPClient(c)
	customerParams := &stripe.CustomerParams{
		Desc:  this.Name,
		Email: this.Email,
		Plan:  "OPMonthly12",
	}
	customerParams.Params.AddMeta("connectedAccount", this.ManagedAccountId)
	customerParams.SetSource(payCardToken)

	toReturn, er := opStripeClient.Customers.New(customerParams)
	if er != nil {
		return toReturn, er
	}

	if len(toReturn.Subs.Values) > 0 {
		sub := toReturn.Subs.Values[0]
		this.PlanSubscriptionId = sub.ID
	}
	this.SubscriptionCustomerId = toReturn.ID

	if err := this.Save(c); err != nil {
		return toReturn, err
	}

	return toReturn, nil
}

func (this *Organization) CancelSubscription(c appengine.Context) (/**stripe.Customer, */error) {
	opStripeClient := StripeOPClient(c)
	_, err := opStripeClient.Subs.Cancel(
		this.PlanSubscriptionId,
		&stripe.SubParams{Customer: this.SubscriptionCustomerId},
	)
	if err != nil {
		fmt.Println("Problem canceling subscription of the organization: ", err)
		return err
	}
	this.PlanSubscriptionId = ""
	this.SubscriptionCustomerId = ""
	er := this.Save(c)
	if er != nil {
		fmt.Println("Problem removing subscription id from storage", er)
		return er
	}
	return err
}

func (this *Organization) HasTransferBankAccount(c appengine.Context) bool {
	return this.TransfersEnabled
}

////// Stripe Testing

func (this *Organization) GenerateTestCardToken(c appengine.Context) string {
	sc := StripeOPClient(c)
	t, err := sc.Tokens.New(&stripe.TokenParams{
		Card: &stripe.CardParams{
			Number: "4242424242424242",
			Month:  "12",
			Year:   "2017",
			CVC:    "123",
		},
	})
	if err != nil {
		fmt.Println("Problem generating test bank token: ", err)
	}
	return t.ID
}

func (this *Organization) GenerateTestBankToken(c appengine.Context) string {
	sc := this.OrganizationStripeClient(c)
	t, err := sc.Tokens.New(&stripe.TokenParams{
		Bank: &stripe.BankAccountParams{
			Country:           "US",
			Currency:          "usd",
			AccountHolderName: "Jane Austen",
			AccountHolderType: "individual",
			Routing:           "110000000",
			Account:           "000123456789",
		},
	})
	if err != nil {
		fmt.Println("Problem generating test bank token: ", err)
	}
	return t.ID
}

////// Authentication
func (this *Organization) Authenticate(c appengine.Context, email, token string) error {
	if err := this.GetByEmail(c, email); err != nil {
		return err
	}
	hashToken := hashFromToken(token)
	c.Debugf("Got token: %s", token)
	c.Debugf("Checking hash: ", hashToken)
	if this.TextCode == "" {
		return fmt.Errorf("No namespace assigned to organization")
	}
	namespaceContext, err := OrgNamespace(c, this.TextCode)
	if err != nil {
		return err
	}
	_, e := memcache.Get(namespaceContext, hashToken)
	if e != nil {
		return e
	}
	return nil
}
func GenerateTestStripeBankAccountToken(c appengine.Context) string {

	return ""
}

func (this *Organization) Login(c appengine.Context, email, password string) (string, error) {
	//Get account by email
	if err := this.GetByEmail(c, email); err != nil {
		return "", err
	}
	//Verify correct password
	if err := bcrypt.CompareHashAndPassword([]byte(this.Password), []byte(password)); err != nil {
		c.Debugf("Problem checking correct password")
		c.Debugf("PW#: %v", this.Password)
		c.Debugf("Attempt: %v", password)
		return "", err
	}
	c.Debugf("checked correct password just fine")
	//Create an auth token to return to the user
	token, hash := newTokenWithHash()
	//Hash the auth token and store in namespaced memcache w/ expiration date
	if err := this.SaveAuthTokenToMemCache(c, hash); err != nil {
		return "", err
	}
	this.currentLoginToken = token
	c.Debugf("Returning token: ", token)
	c.Debugf("Saving hash: ", hash)
	return token, nil
}

func newTokenWithHash() (string, string) {
	randomString := uniuri.NewLen(8)
	hash := hashFromToken(randomString)
	return randomString, hash
}

//This purpose of this 'Hash' is only to alter the token in a way unknown to the client,
//but predictable to the server.
func hashFromToken(token string) string {
	return token
}

////// Password Management
func (this *Organization) ChangePassword(oldP, newP string) error {
	er := bcrypt.CompareHashAndPassword([]byte(this.Password), []byte(oldP))
	if er != nil {
		return er
	}
	if e := this.NewPassword(newP); e != nil {
		return e
	}
	return nil
}

func (this *Organization) NewPassword(newPword string) error {
	if err := ValidatePword(newPword); err != nil {
		return err
	}
	bytes, err := bcrypt.GenerateFromPassword([]byte(newPword), bcrypt.DefaultCost)
	if nil != err {
		panic("User Change Password created error " + err.Error())
	}
	this.Password = string(bytes)
	return nil
}

func (this *Organization) ResetPassword(c appengine.Context) (string, error) {
	randomString := uniuri.NewLen(12)
	var _ = this.NewPassword(randomString)
	if err := this.Save(c); err != nil {
		return "", err
	}
	return randomString, nil
}

func ValidatePword(pword string) error {
	if len(pword) < 7 || len(pword) > 32 {
		return errors.New("Please enter a password between 8 and 32 characters")
	}
	return nil
}

////// Confirmation Code Management
func (this *Organization) GenerateConfirmationCode(c appengine.Context) (string, error) {
	confirmationCode := uniuri.NewLen(8)
	this.Confirmation = confirmationCode
	err := this.Save(c)
	return this.Confirmation, err
}

func (this *Organization) CheckConfirmationCode(c appengine.Context, confirmationCode string) (bool, error) {
	if this.Confirmation == "" {
		return false, fmt.Errorf("No confirmation code saved for account")
	} else if confirmationCode == this.Confirmation {
		return true, nil
	} else {
		return false, nil
	}
}

////// Retrieving

// func (this *Organization) GetByStripeAccount(c appengine.Context, stripeAccount string) error

func (this *Organization) GetByID(c appengine.Context, ID string) error {

	orgKey, e := datastore.DecodeKey(ID)
	if e != nil {
		return e
	}
	err := datastore.Get(c, orgKey, this)
	this.ID = ID
	if err != nil {
		return err
	}
	return nil
}

func (this *Organization) GetByTextCode(c appengine.Context, textCode string) error {
	var orgs []Organization
	textCode = strings.ToLower(textCode)
	q := datastore.NewQuery("Organization").Filter("TextCode =", textCode).Ancestor(orgListKey(c))
	keys, err := q.GetAll(c, &orgs)
	if err != nil {
		return err
	}
	c.Infof("keys: %d", len(keys))
	if len(keys) > 0 {
		orgKey := keys[0]
		if e := datastore.Get(c, orgKey, this); e != nil {
			return e
		}
		this.ID = orgKey.Encode()
		c.Infof("Returning Org: %+v", this)
	} else {
		return fmt.Errorf("No organization found for this text code")
	}
	return nil
}

func (this *Organization) GetByManagedAccountId(c appengine.Context, stripeAcctID string) error {
	var orgs []Organization
	q := datastore.NewQuery("Organization").Filter("ManagedAccountId =", stripeAcctID).Ancestor(orgListKey(c))
	keys, err := q.GetAll(c, &orgs)
	if err != nil {
	}
	c.Infof("keys: %d", len(keys))
	if len(keys) > 0 {
		orgKey := keys[0]
		if e := datastore.Get(c, orgKey, this); e != nil {
			return e
		}
		this.ID = orgKey.Encode()
		c.Infof("Returning Org: %+v", this)
	}
	return nil
}

func (this *Organization) GetBySubscriptionCustomerId(c appengine.Context, stripeID string) error {
	var orgs []Organization
	q := datastore.NewQuery("Organization").Filter("SubscriptionCustomerId =", stripeID).Ancestor(orgListKey(c))
	keys, err := q.GetAll(c, &orgs)
	if err != nil {
	}
	c.Infof("keys: %d", len(keys))
	if len(keys) > 0 {
		orgKey := keys[0]
		if e := datastore.Get(c, orgKey, this); e != nil {
			return e
		}
		this.ID = orgKey.Encode()
		c.Infof("Returning Org: %+v", this)
	}
	return nil
}

// Does not return errors if the object isn't found. If there is no object for the identifier, the ID will be ""
func (this *Organization) GetByEmail(c appengine.Context, email string) error {
	var orgs []Organization
	email = strings.ToLower(email)
	q := datastore.NewQuery("Organization").Filter("Email =", email).Ancestor(orgListKey(c))
	keys, err := q.GetAll(c, &orgs)
	if err != nil {
	}
	c.Infof("keys: %d", len(keys))
	if len(keys) > 0 {
		orgKey := keys[0]
		if e := datastore.Get(c, orgKey, this); e != nil {
			return e
		}
		this.ID = orgKey.Encode()
		c.Infof("Returning Org: %+v", this)
	}
	return nil
}

////// Persistence
func (this *Organization) EncodedKey(c appengine.Context) string {
	return this.ID
}
func (this *Organization) Save(c appengine.Context) error {
	this.Timestamp = time.Now().Unix()
	this.TextCode = strings.ToLower(this.TextCode)
	this.Email = strings.ToLower(this.Email)
	//VALIDATION
	if this.TextCode == "" {
		return fmt.Errorf("Text code not set. Cannot save")
	}
	var myKey *datastore.Key
	myKey, err := datastore.DecodeKey(this.ID)
	if err != nil {
		myKey = datastore.NewKey(c, "Organization", "", 0, orgListKey(c))
	}
	savekey, err := datastore.Put(c, myKey, this)
	if err != nil {
		return err
	}

	this.ID = savekey.Encode()
	return err
}

func (this *Organization) SaveAuthTokenToMemCache(c appengine.Context, token string) error {
	weekAhead := time.Now().AddDate(0, 0, 7)
	duration := weekAhead.Sub(time.Now())
	if this.TextCode == "" {
		return errors.New("Cannot Save: No text code for organization")
	}
	namespaceContext, err := OrgNamespace(c, this.TextCode)
	if err != nil {
		return err
	}
	item := &memcache.Item{
		Key:   token,
		Value: []byte("true"),
	}
	item.Expiration = duration

	if err := memcache.Set(namespaceContext, item); err != nil {
		return errors.New(fmt.Sprintf("Error adding auth code: %v to memcache", err))
	}

	return nil
}

func (this *Organization) GetPuzzleAnswerForNumber(c appengine.Context, phoneNumber string) (int64, error) {
	namespaceContext, err := OrgNamespace(c, this.TextCode)
	if err != nil {
		return 0, err
	}
	item, er := memcache.Get(namespaceContext, phoneNumber)
	if er != nil {
		return 0, er
	}
	stringSum := string(item.Value)
	sum, _ := strconv.ParseInt(stringSum, 10, 64)
	return sum, nil
}

func (this *Organization) SaveToMemCache(c appengine.Context, key, value string) error {
	if this.Email == "" {
		return errors.New("Cannot Save: No email for organization")
	}
	namespaceContext, err := OrgNamespace(c, this.TextCode)
	if err != nil {
		return err
	}

	item := &memcache.Item{
		Key:   key,
		Value: []byte(value),
	}
	if err := memcache.Set(namespaceContext, item); err != nil {
		return errors.New(fmt.Sprintf("Error adding item: %v to memcache", err))
	}

	return nil
}

func orgListKey(c appengine.Context) *datastore.Key {
	//Use a parent key for all organizations to get the advantage of strong consistency
	return datastore.NewKey(c, "OrgList", "default_orgList", 0, nil)
}

func OrgNamespace(c appengine.Context, namespace string) (appengine.Context, error) {
	if namespace == "" {
		return nil, fmt.Errorf("No namespace given")
	}
	return appengine.Namespace(c, namespace)
}

func (this *Organization) GetAllDonors(c appengine.Context) ([]*Donor, error) {
	orgAncestorKey, e := datastore.DecodeKey(this.ID)
	if e != nil {
		return nil, e
	}
	q := datastore.NewQuery("Donation").Ancestor(orgAncestorKey).Project("DonorID").Distinct()
	results := q.Run(c)
	donors := []*Donor{}
	for {
		var d Donation
		_, err := results.Next(&d)
		if err == datastore.Done {
			break
		}
		if err != nil {
			c.Errorf("Running query: %v", err)

			break
		}
		donor := &Donor{}
		if e := donor.GetByID(c, d.DonorID); e != nil {
			c.Errorf("Invalid Donor listed: %v", e)
		} else {
			//We don't want to return payment info (even though it's not dangerous w/o our stripe key)
			donor.StripeCustomerID = ""
			donors = append(donors, donor)
		}
	}
	return donors, nil
}

// Maybe modify this to include date filters

func (this *Organization) GetAllDonations(c appengine.Context) ([]*Donation, error) {
	orgAncestorKey, e := datastore.DecodeKey(this.ID)
	if e != nil {
		return nil, e
	}
	q := datastore.NewQuery("Donation").Filter("Completed =", true).Ancestor(orgAncestorKey).Order("Timestamp")
	var donations []*Donation
	keys, err := q.GetAll(c, &donations)
	if err != nil {
		return nil, err
	}
	//assign id's to all donations
	for i, donation := range donations {
		key := keys[i]
		fmt.Println("iterating:%d", key.Encode())
		donation.ID = key.Encode()
	}
	return donations, nil

}

func (this *Organization) GetMostRecentMessage(c appengine.Context) (*Message, error) {
	orgAncestorKey, e := datastore.DecodeKey(this.ID)
	if e != nil {
		return nil, e
	}
	q := datastore.NewQuery("Message").Ancestor(orgAncestorKey).Order("-Timestamp")
	var messages []*Message
	keys, err := q.GetAll(c, &messages)
	if err != nil {
		return nil, err
	}

	if len(keys) == 0 {
		return nil, nil
	}

	toReturn := messages[0]
	toReturn.ID = keys[0].Encode()
	return toReturn, nil

}

func (this *Organization) GetAllMessages(c appengine.Context) ([]*Message, error) {
	orgAncestorKey, e := datastore.DecodeKey(this.ID)
	if e != nil {
		return nil, e
	}
	q := datastore.NewQuery("Message").Ancestor(orgAncestorKey).Order("-Timestamp")
	var messages []*Message
	keys, err := q.GetAll(c, &messages)
	if err != nil {
		return nil, err
	}
	//assign id's to all messages
	for i, message := range messages {
		key := keys[i]
		fmt.Println("iterating:%d", key.Encode())
		message.ID = key.Encode()
	}
	return messages, nil

}

func (this *Organization) GetAllContacts(c appengine.Context) ([]*Contact, error) {
	orgAncestorKey, e := datastore.DecodeKey(this.ID)
	if e != nil {
		return nil, e
	}
	q := datastore.NewQuery("Contact").Ancestor(orgAncestorKey).Order("Name")
	var contacts []*Contact
	keys, err := q.GetAll(c, &contacts)
	if err != nil {
		return nil, err
	}
	//assign id's to all contacts
	for i, contact := range contacts {
		key := keys[i]
		fmt.Println("iterating:%d", key.Encode())
		contact.ID = key.Encode()
	}
	return contacts, nil

}
