package omnipledge_test

import (
	. "github.com/r1cky1337/omnipledge-subscription"

	"appengine/aetest"
	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"strings"
)

var _ = XDescribe("Contacts", func() {

	XContext("Testing Contact Import", func() {

		var singleName = strings.NewReader(`Name,Phone
		Ricky Kirkendall, (304)610-3672
		Madelyn Harwell, (555)543-5678
		Sam McLaughlin, (304)123-4674`)

		var multiName = strings.NewReader(`First Name, Last Name,Phone
		Ricky, Kirkendall, (304)610-3672
		Madelyn, Harwell, (555)543-5678
		Sam, McLaughlin, (304)123-4674`)

		var missingNumber = strings.NewReader(`First Name, Last Name,Phone
		Ricky, Kirkendall, (304)610-3672
		Madelyn, Harwell,
		Sam, McLaughlin, (304)123-4674`)

		It("Should test firstInstanceOfTermInArray", func() {
			terms := []string{"a", "b", "c"}
			array := []string{"z", "10", "a", "q"}
			notHere := []string{"z", "y", "x"}
			ix := FirstInstanceOfTermInArray(terms, array)
			nix := FirstInstanceOfTermInArray(terms, notHere)
			Expect(nix).To(Equal(-1))
			Expect(ix).To(Equal(2))
		})

		It("Should test PhoneColumnIndex", func() {
			headers := []string{"name", "ricky", "phone"}
			ix := PhoneColumnIndex(headers)
			Expect(ix).To(Equal(2))
		})

		It("Should test parse CSV", func() {
			lines, er := ParseCSV(multiName)
			Ω(er).ShouldNot(HaveOccurred())
			Expect(lines[0][0]).To(Equal("First Name"))
			Expect(len(lines)).To(Equal(4))
		})

		It("Should create a phone column", func() {
			dlines, er := ParseCSV(multiName)
			Ω(er).ShouldNot(HaveOccurred())
			hdrs := dlines[0]
			data := dlines[1:]

			phones := PhoneColumn(hdrs, data)
			fmt.Println(phones)
			Expect(len(phones)).To(Equal(3))

			slines, er := ParseCSV(singleName)
			Ω(er).ShouldNot(HaveOccurred())
			hdrs = slines[0]
			data = slines[1:]

			phones = PhoneColumn(hdrs, data)
			fmt.Println(phones)
			Expect(len(phones)).To(Equal(3))
		})

		It("Should create a name column", func() {
			dlines, er := ParseCSV(multiName)
			Ω(er).ShouldNot(HaveOccurred())
			hdrs := dlines[0]
			data := dlines[1:]

			names := NameColumn(hdrs, data)
			fmt.Println(names)
			Expect(len(names)).To(Equal(3))

			slines, er := ParseCSV(singleName)
			Ω(er).ShouldNot(HaveOccurred())
			hdrs = slines[0]
			data = slines[1:]

			names = NameColumn(hdrs, data)
			fmt.Println(names)
			Expect(len(names)).To(Equal(3))

		})

		It("Should test E164 formatting", func() {
			str := "(304)610-3672"
			t := E164Format(str)
			ok := "+13046103672"
			Expect(t).To(Equal(ok))
		})

		It("Should test number and name map", func() {
			dlines, er := ParseCSV(multiName)
			Ω(er).ShouldNot(HaveOccurred())
			hdrs := dlines[0]
			data := dlines[1:]

			names := NameColumn(hdrs, data)
			numbers := PhoneColumn(hdrs, data)

			nnm, er := NumberNameMap(names, numbers)
			Ω(er).ShouldNot(HaveOccurred())
			Expect(nnm["+13046103672"]).To(Equal("Ricky Kirkendall"))

			mlines, er := ParseCSV(missingNumber)
			Ω(er).ShouldNot(HaveOccurred())
			hdrs = mlines[0]
			data = mlines[1:]
			names = NameColumn(hdrs, data)
			numbers = PhoneColumn(hdrs, data)
			nnm, er = NumberNameMap(names, numbers)
			Ω(er).ShouldNot(HaveOccurred())
			Expect(len(nnm)).To(Equal(2))
		})

		It("Should generate maps for contact name and DS ID using the number as key", func() {
			contacts := make([]*Contact, 3)
			contacts[0] = &Contact{
				Name:  "Ricky Kirkendall",
				Phone: "+13046103672",
				ID:    "A",
			}
			contacts[1] = &Contact{
				Name:  "Sam McLaughlin",
				Phone: "+13043333333",
				ID:    "B",
			}
			contacts[2] = &Contact{
				Name:  "Madelyn Harwell",
				Phone: "+13045555555",
				ID:    "C",
			}

			nameMap, keyMap := MapSavedContactNameAndID(contacts)

			exNameMap := map[string]string{}
			exNameMap["+13045555555"] = "Madelyn Harwell"
			exNameMap["+13043333333"] = "Sam McLaughlin"
			exNameMap["+13046103672"] = "Ricky Kirkendall"

			exKeyMap := map[string]string{}
			exKeyMap["+13045555555"] = "C"
			exKeyMap["+13043333333"] = "B"
			exKeyMap["+13046103672"] = "A"

			for number, name := range nameMap {
				Expect(name).To(Equal(exNameMap[number]))
			}
			for number, key := range keyMap {
				Expect(key).To(Equal(exKeyMap[number]))
			}

		})

		It("Should test contact save", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			contact := &Contact{
				Name:  "A",
				Phone: "B",
			}
			contact.OrgID = org.ID

			saveEr := contact.Save(context)
			Ω(saveEr).ShouldNot(HaveOccurred())
			Expect(contact.ID).ToNot(Equal(""))
		})

		It("Should test get all contacts", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			contactA := &Contact{
				Name:  "A",
				Phone: "AA",
				OrgID: org.ID,
			}

			contactB := &Contact{
				Name:  "B",
				Phone: "BB",
				OrgID: org.ID,
			}

			e := contactA.Save(context)
			Ω(e).ShouldNot(HaveOccurred())
			e = contactB.Save(context)
			Ω(e).ShouldNot(HaveOccurred())

			contacts, getEr := org.GetAllContacts(context)
			Ω(getEr).ShouldNot(HaveOccurred())
			Expect(len(contacts)).To(Equal(2))
			Expect(contacts[0].Name).To(Equal("A"))
			Expect(contacts[1].Name).To(Equal("B"))

		})

		It("Should test contact import and sync", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			syncEr := SyncImportedAndExistingContacts(singleName, org, context)
			Ω(syncEr).ShouldNot(HaveOccurred())

			contacts, getEr := org.GetAllContacts(context)
			Ω(getEr).ShouldNot(HaveOccurred())
			Expect(len(contacts)).To(Equal(3))
			for _, c := range contacts {
				fmt.Println(c)
			}
		})

		It("Should test getting contact by ID", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			contact := &Contact{
				Name:  "A",
				Phone: "B",
			}
			contact.OrgID = org.ID

			_ = contact.Save(context)

			gotContact := &Contact{}
			getEr := gotContact.GetByID(context, contact.ID)
			Ω(getEr).ShouldNot(HaveOccurred())

			Expect(gotContact.Name).To(Equal("A"))
		})

		It("Should test getting contact by phone number", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			contact := &Contact{
				Name:  "A",
				Phone: "B",
			}
			contact.OrgID = org.ID

			_ = contact.Save(context)

			gotContact := &Contact{}
			getEr := gotContact.GetByPhone(context, org.ID, contact.Phone)
			Ω(getEr).ShouldNot(HaveOccurred())

			Expect(gotContact.Name).To(Equal("A"))
		})

		It("Should test deleting a contact", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			contact := &Contact{
				Name:  "A",
				Phone: "B",
			}
			contact.OrgID = org.ID

			_ = contact.Save(context)
			delEr := contact.Delete(context)
			Ω(delEr).ShouldNot(HaveOccurred())

			gotContact := &Contact{}
			getEr := gotContact.GetByID(context, contact.ID)
			Ω(getEr).Should(HaveOccurred())

		})

	})

})
