package omnipledge_test

import (
	"appengine/aetest"
	//"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/r1cky1337/omnipledge-subscription"
	"time"
)

var _ = Describe("Message", func() {
	Context("Testing Messages", func() {

		It("Should test message save", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			message := &Message{
				Body:       "Hello friend",
				Live:       true,
				Recipients: []string{"1", "2", "3"},
			}
			message.OrgID = org.ID

			saveEr := message.Save(context)
			Ω(saveEr).ShouldNot(HaveOccurred())
			Expect(message.ID).ToNot(Equal(""))
		})

		It("Should test get all messages", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			messageA := &Message{
				Body:  "A",
				OrgID: org.ID,
			}

			messageB := &Message{
				Body:  "B",
				OrgID: org.ID,
			}

			e := messageA.Save(context)
			Ω(e).ShouldNot(HaveOccurred())
			time.Sleep(2000 * time.Millisecond)
			e = messageB.Save(context)
			Ω(e).ShouldNot(HaveOccurred())

			messages, getEr := org.GetAllMessages(context)
			Ω(getEr).ShouldNot(HaveOccurred())
			Expect(len(messages)).To(Equal(2))
			Expect(messages[1].Body).To(Equal("A"))
			Expect(messages[0].Body).To(Equal("B"))

		})

		It("Should test getting message by ID", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			message := &Message{
				Body: "A",
			}
			message.OrgID = org.ID

			_ = message.Save(context)

			gotMessage := &Message{}
			getEr := gotMessage.GetByID(context, message.ID)
			Ω(getEr).ShouldNot(HaveOccurred())

			Expect(gotMessage.Body).To(Equal("A"))
		})

		It("Should test get most recent message", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())

			messageA := &Message{
				Body:  "A",
				OrgID: org.ID,
			}

			messageB := &Message{
				Body:  "B",
				OrgID: org.ID,
			}

			e := messageA.Save(context)
			Ω(e).ShouldNot(HaveOccurred())
			time.Sleep(2000 * time.Millisecond)
			e = messageB.Save(context)
			Ω(e).ShouldNot(HaveOccurred())

			message, getEr := org.GetMostRecentMessage(context)
			Ω(getEr).ShouldNot(HaveOccurred())
			Expect(message.Body).To(Equal("B"))
			Expect(message.ID).ToNot(Equal(""))

		})
	})
})
