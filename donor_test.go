package omnipledge_test

import (
	"appengine/aetest"
	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/r1cky1337/omnipledge-subscription"
	"time"
)

var _ = Describe("Donor", func() {

	var (
		testEmail = "ricky@syntropy.io"
		testPhone = "3046103672"
		noID      int64
		newDonor  *Donor
	)
	Context("Testing Donor", func() {
		newDonor = &Donor{
			Name:  "Ricky Kirkendall",
			Phone: testPhone,
			Zip:   "25309",
			Email: testEmail,
		}
		noID = 0

		It("Should save a new donor object", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			err := newDonor.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(newDonor.ID).ToNot(Equal(noID))
		})
		It("Should get donor by email", func() {
			testDonor := &Donor{}
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			_ = newDonor.Save(context)
			err := testDonor.GetByEmail(context, testEmail)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(testDonor.Phone).To(Equal(testPhone))
		})
		It("Should fail to get donor by email", func() {
			testDonor := &Donor{}
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			_ = newDonor.Save(context)
			err := testDonor.GetByEmail(context, "fish")
			Ω(err).ShouldNot(HaveOccurred())
			Expect(testDonor.Phone).ToNot(Equal(testPhone))
		})
		It("Should get donor by phone number", func() {
			testDonor := &Donor{}
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			newDonor.PhoneVerified = true
			_ = newDonor.Save(context)
			err := testDonor.GetByPhone(context, testPhone)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(testDonor.Email).To(Equal(testEmail))
		})
		It("Should fail to get donor with unverified phone number", func() {
			testDonor := &Donor{}
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			newDonor.PhoneVerified = false
			_ = newDonor.Save(context)
			_ = testDonor.GetByPhone(context, testPhone)
			Expect(testDonor.ID).To(Equal(""))
			Expect(testDonor.Email).ToNot(Equal(testEmail))
		})
		It("Should fail to get donor by phone number", func() {
			testDonor := &Donor{}
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			_ = newDonor.Save(context)
			err := testDonor.GetByPhone(context, "fish")
			Ω(err).ShouldNot(HaveOccurred())
			Expect(testDonor.Email).ToNot(Equal(testEmail))
		})
		It("Should get donor by id", func() {
			testDonor := &Donor{}
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			_ = newDonor.Save(context)
			err := testDonor.GetByID(context, newDonor.ID)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(testDonor.Email).To(Equal(testEmail))
		})
		It("Should fail to get donor by id", func() {
			testDonor := &Donor{}
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			_ = newDonor.Save(context)
			err := testDonor.GetByID(context, "1337")
			Ω(err).Should(HaveOccurred())
			Expect(testDonor.Email).ToNot(Equal(testEmail))
		})
		It("Should create a new donation", func() {
			testOrg := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			testOrg.NewPassword("testing123")
			_ = testOrg.Save(context)
			_ = newDonor.Save(context)
			testDonation, err := newDonor.NewDonation(context, testOrg)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(testDonation.DonorID).To(Equal(newDonor.ID))
			Expect(testDonation.OrgID).To(Equal(testOrg.ID))
		})
		It("Should generate a test card token from stripe", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			token, err := newDonor.CreateTestCardToken(context)
			Ω(err).ShouldNot(HaveOccurred())
			fmt.Println(token)
			Expect(token).ToNot(Equal(""))
		})
		FIt("Should charge a donation to new donor", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()

			newOrg := &Organization{
				Name:     "MyOrg",
				Email:    "a@a.com",
				TextCode: "test",
				Verified: true,
			}
			signupReq := SignupRequest{
				FirstName:       "Ricky",
				LastName:        "Kirkendall",
				TextCode:        "textcode",
				DobDay:          11,
				DobMonth:        6,
				DobYear:         1992,
				IsBusiness:      false,
				SSNLastFour:     "1234",
				TOSTimeAccepted: time.Now().Unix(),
				TOSIPAccepted:   "127.0.0.1",
				AddressLineOne:  "1 Mainstreet Ln",
				AddressCity:     "Morgantown",
				AddressState:    "WV",
				AddressZip:      "26505",
			}
			acct, _ := newOrg.CreateManagedAccount(context, signupReq)
			newOrg.SetAccountMangedAccountFields(context, acct)
			cardToken := newOrg.GenerateTestCardToken(context)
			_, e := newOrg.SubscribeWithPayment(context, cardToken)
			Ω(e).ShouldNot(HaveOccurred())
			_ = newOrg.Save(context)
			_ = newDonor.Save(context)
			fmt.Println("org id ", newOrg.ID)
			//TODO: Change test to decouple donation with donor.
			testDonation := &Donation{}
			testDonation.OrgID = newOrg.ID
			testDonation.Amount = 30001
			testDonation.DonorID = newDonor.ID
			err := testDonation.Save(context)
			Ω(err).ShouldNot(HaveOccurred())
			token, er := newDonor.CreateTestCardToken(context)
			Ω(er).ShouldNot(HaveOccurred())
			e = newDonor.ChargeWebCustomer(context, token, testDonation)
			Ω(e).ShouldNot(HaveOccurred())
			Expect(testDonation.StripeChargeID).ToNot(Equal(""))
			var total64 int64 = 30001
			var times64 int64 = 1
			Expect(newDonor.TotalDonated).To(Equal(total64))
			Expect(newDonor.DonationsMade).To(Equal(times64))

			//Check that the transfer occured on the managed stripe account
			total, e := newOrg.DonationTotalThisPayPeriod(context)
			Ω(e).ShouldNot(HaveOccurred())
			Expect(total).To(Equal(int64(300001)))

		})
	})
})
