package omnipledge

import (
	"appengine"
	"appengine/urlfetch"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	SMSStateSelectOrganization  = "organization"
	SMSStateDonationAmount      = "amount"
	SMSStatePaymentInfo         = "payment"
	SMSStateVerifyDonation      = "verify"
	SMSStateSolvePuzzle         = "puzzle"
	SMSStateThankYou            = "thanks"
	SMSStateInvalidOrganization = "invalidOrg"
	SMSStateInvalidAmount       = "invalidAmount"
	SMSStateEmpty               = ""
)

func SendTextMessage(c appengine.Context, to, message string) {
	accountSid := "AC7255c7e74557bc0d7f2244e2ed867744"
	authToken := "c7f5c266903bc3d0452bb9eb49853ea1"
	urlStr := "https://api.twilio.com/2010-04-01/Accounts/" + accountSid + "/Messages.json"

	client := urlfetch.Client(c)

	v := url.Values{}
	v.Set("To", to)
	v.Set("From", OPTwilioNumber)
	v.Set("Body", message)
	rb := *strings.NewReader(v.Encode())
	req, _ := http.NewRequest("POST", urlStr, &rb)
	req.SetBasicAuth(accountSid, authToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	_, err := client.Do(req)
	if err != nil {
		LogError(c, "Problem sending SMS", err)
	}
}

func SMSRespond(c appengine.Context, res http.ResponseWriter, req *http.Request, from, body, accountSid string) {
	state := getSMSState(req)
	//A
	body = strings.TrimSpace(body)
	body = strings.ToLower(body)

	if body == "update" {		// If UPDATE is entered, send payment update link without state change
		handleUpdateState(c, res, req, body, from)
	} else if state == SMSStateEmpty || state == SMSStateSelectOrganization {
		handleSelectionState(c, res, body, from)
	} else if state == SMSStateDonationAmount {
		if body == "back" {
			//SendTextMessage(c, from, "Donation cancelled")
			setSMSState(res, SMSStateEmpty)
		} else {
			handleAmountState(c, res, req, body, from)
		}
	} else if state == SMSStateVerifyDonation {
		if body == "back" {
			//SendTextMessage(c, from, "Donation cancelled")
			setSMSState(res, SMSStateEmpty)
		} else {
			handleVerifyState(c, res, req, body, from)
		}
	}
}

func handleUpdateState(c appengine.Context, res http.ResponseWriter, req *http.Request, body, from string) {
	existingDonor := &Donor{}
	if er := existingDonor.GetByPhone(c, from); er != nil {
		SendTextMessage(c, from, "Unable to identify donor.")
	}
	url := fmt.Sprintf("%sdonorupdate?drID=%s", OPBaseURL, existingDonor.ID)
	shortCode, shortenEr := ShortURLCode(c, url)
	if shortenEr != nil {
		LogError(c, "Problem shortening url for sms", shortenEr)
		SendTextMessage(c, from, "Unable to display payment update form. Please try again")
	}
	shortUrl := fmt.Sprintf("%sd/%s", OPBaseURL, shortCode)
	SendTextMessage(c, from, "Please update your payment card info in the following page: " + shortUrl)
}

func handleSelectionState(c appengine.Context, res http.ResponseWriter, body, from string) {
	organization := &Organization{}
	//F
	if err := organization.GetByTextCode(c, body); err != nil {
		SendTextMessage(c, from, "Cannot find a cause with that text code. Please ensure that your text code is correct and try again.")
		setSMSState(res, SMSStateSelectOrganization)
	} else if organization.PlanSubscriptionId == "" {
		SendTextMessage(c, from, "There is an organization with that text code, but they haven't created subscription yet.")
		setSMSState(res, SMSStateSelectOrganization)
	} else {
		//->B
		message := fmt.Sprintf("How much would you like to donate to %s? Reply with a number like 15 to give $15. (send BACK to start over)", organization.Name)
		SendTextMessage(c, from, message)
		setSMSOrg(res, organization.TextCode)
		setSMSState(res, SMSStateDonationAmount)
	}
}

func handleAmountState(c appengine.Context, res http.ResponseWriter, req *http.Request, body, from string) {
	_, err := strconv.Atoi(body)
	if err != nil {
		//G
		SendTextMessage(c, from, "Please enter an exact dollar amount. For example, enter 15 to donate $15 dollars.")
	} else {
		//B
		setSMSAmount(res, body)

		//If donor can be found by number, use existing stripe customer
		existingDonor := &Donor{}
		if er := existingDonor.GetByPhone(c, from); er != nil {
			SendTextMessage(c, from, "Unable to create donation. Please try again")
			setSMSState(res, SMSStateEmpty)
		}
		if existingDonor.ID != "" {
			//Account exists
			// ->D
			setSMSState(res, SMSStateVerifyDonation)

			//Create a puzzle that uses the phone number+"puzzle" as a key in
			//organization memcache and store the answer as the key
			rand1 := RandomDigit()
			rand2 := RandomDigit()
			orgTextCode := getSMSOrg(req)
			organization := &Organization{}
			_ = organization.GetByTextCode(c, orgTextCode)
			sum := rand1 + rand2
			sumString := strconv.FormatInt(sum, 10)
			if err := organization.SaveToMemCache(c, from, sumString); err != nil {
				SendTextMessage(c, from, "Unable to create donation. Please try again")
				setSMSState(res, SMSStateEmpty)
			}
			message := fmt.Sprintf("Thanks! Please verify your transaction by completing this simple puzzle: %d + %d =  (send BACK to start over)",
				rand1,
				rand2,
			)
			SendTextMessage(c, from, message)
		} else {
			//Empty state and set and provide link for transaction to be finished over web
			setSMSState(res, SMSStateEmpty)
			organization := &Organization{}
			textCode := getSMSOrg(req)
			_ = organization.GetByTextCode(c, textCode)
			donation := &Donation{}
			donation.OrgID = organization.ID
			donation.PhoneVerified = true
			donation.Phone = from
			donation.Method = OPDonationMethodSMS
			amountString := body
			amountDollars, _ := strconv.ParseInt(amountString, 10, 64)
			amountCents := amountDollars * 100
			donation.Amount = amountCents
			if err := donation.Save(c); err != nil {
				SendTextMessage(c, from, "Unable to create donation. Please try again")
				setSMSState(res, SMSStateEmpty)
			}
			url := fmt.Sprintf("%sdonation?dID=%s&oID=%s", OPBaseURL, donation.ID, donation.OrgID)
			shortCode, shortenEr := ShortURLCode(c, url)
			if shortenEr != nil {
				LogError(c, "Problem shortening url for sms", shortenEr)
				SendTextMessage(c, from, "Unable to create donation. Please try again")
				setSMSState(res, SMSStateEmpty)
			}
			shortUrl := fmt.Sprintf("%sd/%s", OPBaseURL, shortCode)

			message := fmt.Sprintf("Please tap here to complete your donation (you only have to do this once!) %s", shortUrl)
			SendTextMessage(c, from, message)
		}
	}
}
func handleVerifyState(c appengine.Context, res http.ResponseWriter, req *http.Request, body, from string) {
	answerString := body
	textCode := getSMSOrg(req)
	organization := &Organization{}
	_ = organization.GetByTextCode(c, textCode)
	answer, err := organization.GetPuzzleAnswerForNumber(c, from)
	if err != nil {
		SendTextMessage(c, from, "Unable to create donation. Please try again")
		return
	}
	answerNumber, _ := strconv.ParseInt(answerString, 10, 64)
	if answerNumber == answer {
		//Initiate mobile donation

		//Get donor
		existingDonor := &Donor{}
		if er := existingDonor.GetByPhone(c, from); er != nil {
			SendTextMessage(c, from, "Unable to create donation. Please try again")
			setSMSState(res, SMSStateEmpty)
			return
		}
		//Get Organization
		organization := &Organization{}
		textCode := getSMSOrg(req)
		_ = organization.GetByTextCode(c, textCode)

		//Create donation
		donation, _ := existingDonor.NewDonation(c, organization)
		amountString := getSMSAmount(req)
		amountDollars, _ := strconv.ParseInt(amountString, 10, 64)
		amountCents := amountDollars * 100
		donation.Amount = amountCents
		donation.Method = OPDonationMethodSMS
		if err := existingDonor.ChargeMobileCustomer(c, donation); err != nil {
			url := fmt.Sprintf("%sdonorupdate?drID=%s", OPBaseURL, existingDonor.ID)
			shortCode, shortenEr := ShortURLCode(c, url)
			if shortenEr != nil {
				LogError(c, "Problem shortening url for sms", shortenEr)
				SendTextMessage(c, from, "Unable to display payment update form. Please try again")
				return
			}
			shortUrl := fmt.Sprintf("%sd/%s", OPBaseURL, shortCode)
			SendTextMessage(c, from, "Unable to create donation. Please reenter your payment card info in the following page: " + shortUrl)
			setSMSState(res, SMSStateEmpty)
			return
		}
		SendTextMessage(c, from, "Thank you for your donation!")
		setSMSState(res, SMSStateEmpty)

	} else {
		rand1 := RandomDigit()
		rand2 := RandomDigit()
		orgTextCode := getSMSOrg(req)
		organization := &Organization{}
		_ = organization.GetByTextCode(c, orgTextCode)
		sum := rand1 + rand2
		sumString := strconv.FormatInt(sum, 10)
		if err := organization.SaveToMemCache(c, from, sumString); err != nil {
			SendTextMessage(c, from, "Unable to create donation. Please try again")
			setSMSState(res, SMSStateEmpty)
		}
		message := fmt.Sprintf("Sorry, the puzzle wasn't solved correctly. Try this one: %d + %d = ",
			rand1,
			rand2,
		)
		SendTextMessage(c, from, message)
	}
}

func RandomDigit() int64 {
	time.Sleep(time.Millisecond * 10)
	rand.Seed(time.Now().UnixNano())
	return rand.Int63n(10)
}

func setSMSAmount(res http.ResponseWriter, amountString string) {
	http.SetCookie(res, &http.Cookie{Name: "amount", Value: amountString})
}
func getSMSAmount(req *http.Request) string {
	cookie, err := req.Cookie("amount")
	if err != nil {
		cookie = &http.Cookie{}
	}
	return cookie.Value
}

func setSMSOrg(res http.ResponseWriter, textCode string) {
	http.SetCookie(res, &http.Cookie{Name: "org", Value: textCode})
}
func getSMSOrg(req *http.Request) string {
	cookie, err := req.Cookie("org")
	if err != nil {
		cookie = &http.Cookie{}
	}
	return cookie.Value
}

func getSMSState(req *http.Request) string {
	cookie, err := req.Cookie("state")
	if err != nil {
		cookie = &http.Cookie{}
	}
	return cookie.Value
}
func setSMSState(res http.ResponseWriter, state string) {
	http.SetCookie(res, &http.Cookie{Name: "state", Value: state})
}
