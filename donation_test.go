package omnipledge_test

import (
	"appengine/aetest"
	//"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/r1cky1337/omnipledge-subscription"
	"time"
)

var _ = Describe("Donation", func() {
	var (
		newOrg          *Organization
		newDonor        *Donor
		donationRequest DonationRequest
		noID            int64
		orgEmail        string

		testName   string
		testAmount int64
		testPhone  string
		testEmail  string
		testZip    string
	)
	Context("Testing Donation", func() {
		noID = 0
		orgEmail = "e@mail.com"
		newOrg = &Organization{
			Name:  "Orgname",
			Email: orgEmail,
		}
		newDonor = &Donor{
			Name:  "Ricky Kirkendlal",
			Phone: "3046103672",
			Email: "ricky@raisinapp.com",
			Zip:   "25309",
		}

		testName = "Timmy Donorson"
		testZip = "25309"
		testAmount = 2300
		testPhone = "3047447886"
		testEmail = "donor@phonedone.com"

		donationRequest = DonationRequest{
			Name:   testName,
			Zip:    testZip,
			Amount: testAmount,
			Phone:  testPhone,
			Email:  testEmail,
		}

		It("Should update donation object with request struct", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			donation := &Donation{}
			err := donation.UpdateWithRequest(context, donationRequest)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(donation.Name).To(Equal(testName))
			Expect(donation.Amount).To(Equal(testAmount))
			Expect(donation.Phone).To(Equal(testPhone))
			Expect(donation.Email).To(Equal(testEmail))
		})

		It("Should test getting a donation by charge ID", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()
			donation := &Donation{}
			org := &Organization{
				Name:     "Red Cross",
				Email:    "red@cross.com",
				TextCode: "test",
			}
			org.NewPassword("testing123")
			_ = org.Save(context)
			orgSaveEr := org.Save(context)
			Ω(orgSaveEr).ShouldNot(HaveOccurred())
			testStripeToken := "stripeTokenasdfadf"
			donation.StripeChargeID = testStripeToken
			donation.OrgID = org.ID
			testName := "timmy donorson"
			donation.Name = testName
			erSave := donation.Save(context)
			Ω(erSave).ShouldNot(HaveOccurred())
			Expect(donation.ID).ToNot(Equal(""))
			blankDonation := &Donation{}
			erGet := blankDonation.GetByCharge(context, org.ID, testStripeToken)
			Ω(erGet).ShouldNot(HaveOccurred())
			Expect(blankDonation.Name).To(Equal(testName))
		})

		It("Should test getting all completed donations before timestamp", func() {
			context, _ := aetest.NewContext(nil)
			defer context.Close()

			old := 3
			i := 0
			for i < old {
				org := &Organization{
					Name:     "Red Cross",
					Email:    "red@cross.com",
					TextCode: "test",
				}
				org.NewPassword("testing123")
				_ = org.Save(context)
				donation := &Donation{}
				donation.OrgID = org.ID
				donation.Completed = true
				saveEr := donation.Save(context)
				Ω(saveEr).ShouldNot(HaveOccurred())
				i++
			}

			oldIncomplete := 4
			i = 0
			for i < oldIncomplete {
				org := &Organization{
					Name:     "Red Cross",
					Email:    "red@cross.com",
					TextCode: "test",
				}
				org.NewPassword("testing123")
				_ = org.Save(context)
				donation := &Donation{}
				donation.OrgID = org.ID
				donation.Completed = false
				saveEr := donation.Save(context)
				Ω(saveEr).ShouldNot(HaveOccurred())
				i++
			}

			time.Sleep(4000 * time.Millisecond)

			recent := 5
			i = 0
			for i < recent {
				org := &Organization{
					Name:     "Red Cross",
					Email:    "red@cross.com",
					TextCode: "test",
				}
				org.NewPassword("testing123")
				_ = org.Save(context)
				donation := &Donation{}
				donation.OrgID = org.ID
				donation.Completed = true
				saveEr := donation.Save(context)
				Ω(saveEr).ShouldNot(HaveOccurred())
				i++
			}

			threeSecondsAgo := time.Now().Unix() - int64(3)

			donations, _ := DonationsOlderThan(context, threeSecondsAgo)
			Expect(len(donations)).To(Equal(oldIncomplete))
		})
	})
})
