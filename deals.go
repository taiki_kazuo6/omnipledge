package omnipledge

import (
	"appengine"
	"appengine/datastore"
	//"fmt"
)

type Deal struct {
	ID              string `datastore:"-"`
	OrganizationID  string
	DonationPercent float64
	Affiliate       bool
	DonationFee     int64
	AffiliateEmail  string
}

func (this *Deal) GetByID(c appengine.Context, ID string) error {

	dealKey, e := datastore.DecodeKey(ID)
	if e != nil {
		return e
	}
	err := datastore.Get(c, dealKey, this)
	this.ID = ID
	if err != nil {
		return err
	}
	return nil
}

func (this *Deal) GetByOrgID(c appengine.Context, OrgID string) error {
	var deals []Deal
	q := datastore.NewQuery("Deal").
		Ancestor(dealListKey(c)).
		Filter("OrganizationID = ", OrgID)
	keys, err := q.GetAll(c, &deals)
	if err != nil {
		return err
	}
	c.Infof("keys returned by GetByOrgID: %d", len(keys))

	if len(keys) > 0 {
		dealKey := keys[0]
		if e := datastore.Get(c, dealKey, this); e != nil {
			return e
		}
		this.ID = dealKey.Encode()
		c.Infof("Returning Deal: %+v", this)
	} else {
		c.Debugf("No Deal exists for this OrgID")
	}
	return nil
}

func (this *Deal) Save(c appengine.Context) error {
	var myKey *datastore.Key
	myKey, err := datastore.DecodeKey(this.ID)
	if err != nil {
		myKey = datastore.NewKey(c, "Deal", "", 0, dealListKey(c))
	}
	savekey, err := datastore.Put(c, myKey, this)
	if err != nil {
		return err
	}

	this.ID = savekey.Encode()
	return err
}

func dealListKey(c appengine.Context) *datastore.Key {
	//Use a parent key for all organizations to get the advantage of strong consistency
	return datastore.NewKey(c, "DealList", "default_DealList", 0, nil)
}
