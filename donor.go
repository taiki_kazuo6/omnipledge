package omnipledge

import (
	"appengine"
	"appengine/datastore"
	"fmt"
	"github.com/stripe/stripe-go"
	// "github.com/stripe/stripe-go/client"
	"math"
	"time"
)

type Donor struct {
	ID                             string `datastore:"-"`
	Name                           string
	Phone                          string
	Email                          string
	Zip                            string
	StripeCustomerID               string
	DonationsMade                  int64
	TotalDonated                   int64 //In cents
	Timestamp                      int64
	PhoneVerified                  bool
	PaymentInformationNeedsUpdated bool
}

func (this *Donor) NewDonation(c appengine.Context, org *Organization) (*Donation, error) {
	if this.ID == "" {
		return &Donation{}, fmt.Errorf("Unsaved Donor cannot create a donation")
	}
	toReturn := &Donation{
		DonorID:       this.ID,
		OrgID:         org.ID,
		Name:          this.Name,
		Email:         this.Email,
		Phone:         this.Phone,
		PhoneVerified: this.PhoneVerified,
	}
	if err := toReturn.Save(c); err != nil {
		return &Donation{}, err
	}
	return toReturn, nil
}
func (this *Donor) CreateTestCardToken(c appengine.Context) (string, error) {
	stripeClient := StripeOPClient(c)
	params := &stripe.TokenParams{
		Card: &stripe.CardParams{
			Name:   "George Costanza",
			Number: "4242424242424242",
			Year:   "2020",
			Month:  "12",
		},
	}

	token, err := stripeClient.Tokens.New(params)
	if err != nil {
		return "", err
	}
	return token.ID, nil
}

//Finds an existing donor through an inputted phone number. Assign the card token to their
// existing stripe account and create a new charge from the donation.

//Use when a full donation has been submitted through the web, but the phone number matches
// an existing donor.

//Finds an exisiting donor from the phone number and charges their existing customer token.

//Use for returning SMS donors via Twilio webhooks and DO NOT call from within a public route.

//Needs unit tests
func (this *Donor) ChargeMobileCustomer(c appengine.Context, donation *Donation) error {
	//VALIDATION
	if valEr := this.valCharge(donation); valEr != nil {
		return valEr
	}

	if this.StripeCustomerID == "" {
		return fmt.Errorf("Stripe Customer ID not found for donor.")
	}
	//create a charge with the following metadata
	doneeOrg := &Organization{}
	if getOrgErr := doneeOrg.GetByID(c, donation.OrgID); getOrgErr != nil {
		return getOrgErr
	}
	description := fmt.Sprintf("%s donated to %s", this.Name, doneeOrg.Name)
	chargeParams := &stripe.ChargeParams{
		Amount:    uint64(donation.Amount),
		Customer:  this.StripeCustomerID,
		Statement: doneeOrg.TextCode,
		Desc:      description,
		Email:     this.Email,
		Currency:  "usd",
		Dest:      doneeOrg.ManagedAccountId,
	}

	shouldTakeFee := doneeOrg.DonationShouldTakeFee(donation.Amount, c)

	chargeEr := this.charge(c, donation, chargeParams, shouldTakeFee)
	if chargeEr != nil {
		return chargeEr
	}

	return nil
}

// Creates a new stripe customer with card token and donor info and builds a charge from the donation.
// Use for first time customers.
func (this *Donor) ChargeWebCustomer(c appengine.Context, stripeCardToken string, donation *Donation) error {
	//VALIDATION

	if valEr := this.valCharge(donation); valEr != nil {
		return valEr
	}

	//Stripe: Charge saved customer or create new saved customer
	stripeClient := StripeOPClient(c)
	params := &stripe.CustomerParams{
		Email: this.Email,
		Desc:  this.Email,
	}
	params.SetSource(stripeCardToken)
	//see if donor stripe customer id exists
	if this.StripeCustomerID == "" {
		customer, err := stripeClient.Customers.New(params)
		if err != nil {
			return err
		}
		this.StripeCustomerID = customer.ID
		if e := this.Save(c); e != nil {
			return e
		}
	} else {
		//if it does, update existing stripe customer with new card token
		_, e := stripeClient.Customers.Update(this.StripeCustomerID, params)
		if e != nil {
			return e
		}
	}

	fmt.Println("customer id:", this.StripeCustomerID)

	doneeOrg := &Organization{}
	if getOrgErr := doneeOrg.GetByID(c, donation.OrgID); getOrgErr != nil {
		return getOrgErr
	}

	// Calculate application fee
	shouldTakeFee := doneeOrg.DonationShouldTakeFee(donation.Amount, c)

	// Set charge destination

	// Create a charge with the following metadata

	description := fmt.Sprintf("%s donated to %s", this.Name, doneeOrg.Name)
	chargeParams := &stripe.ChargeParams{
		Amount:    uint64(donation.Amount),
		Customer:  this.StripeCustomerID,
		Statement: doneeOrg.TextCode,
		Desc:      description,
		Email:     this.Email,
		Currency:  "usd",
		Dest:      doneeOrg.ManagedAccountId,
	}

	chargeEr := this.charge(c, donation, chargeParams, shouldTakeFee)
	if chargeEr != nil {
		return chargeEr
	}

	return nil
}

func Round(n float64) int64 {
	n = n + 0.5
	return int64(math.Floor(n))
}

func (this *Donor) charge(c appengine.Context,
	donation *Donation,
	params *stripe.ChargeParams,
	shouldTakeFee bool) error {

	if shouldTakeFee {
		unroundedFee := (float64(donation.Amount) * OPSubscriptionDonationPercentage) + float64(OPSubscriptionDonationTransactionFee)
		fee := Round(unroundedFee)
		params.Fee = uint64(fee)
	}

	stripeClient := StripeOPClient(c)
	charge, chargeErr := stripeClient.Charges.New(params)
	if chargeErr != nil {
		return chargeErr
	}
	//Set the charge id to the donation
	donation.StripeChargeID = charge.ID
	donation.Completed = true
	donation.Created = time.Now().Unix()
	_ = donation.Save(c)

	//Set donor metadata
	this.TotalDonated = this.TotalDonated + donation.Amount
	this.DonationsMade++
	_ = this.Save(c)

	return nil
}

func (this *Donor) valCharge(donation *Donation) error {
	if this.ID != donation.DonorID {
		return fmt.Errorf("Donation's donor ID does not match")
	}
	if donation.OrgID == "" {
		return fmt.Errorf("Donation does not include the Organization key")
	}
	if donation.Amount == 0 {
		return fmt.Errorf("No amount specified for donation")
	}
	return nil
}

////// Retrieving
// These methods do not return errors if the object isn't found.
// If there is no object for the identifier, the ID will be ""
func (this *Donor) GetByID(c appengine.Context, ID string) error {
	donorKey, e := datastore.DecodeKey(ID)
	if e != nil {
		return e
	}
	err := datastore.Get(c, donorKey, this)
	this.ID = ID
	if err != nil {
		return err
	}
	return nil
}

func (this *Donor) GetByPhone(c appengine.Context, phone string) error {
	var donors []Donor
	q := datastore.NewQuery("Donor").Filter("Phone =", phone).Filter("PhoneVerified =", true).Ancestor(donorListKey(c))
	keys, err := q.GetAll(c, &donors)
	if err != nil {
		return err
	}
	c.Infof("keys: %d", len(keys))
	if len(keys) > 0 {
		donorKey := keys[0]
		if e := datastore.Get(c, donorKey, this); e != nil {
			return e
		}
		this.ID = donorKey.Encode()
		c.Infof("Returning Donor: %+v", this)
	}
	return nil
}

func (this *Donor) GetByEmail(c appengine.Context, email string) error {
	var donors []Donor
	q := datastore.NewQuery("Donor").Filter("Email =", email).Ancestor(donorListKey(c))
	keys, err := q.GetAll(c, &donors)
	if err != nil {
	}
	c.Infof("keys: %d", len(keys))
	if len(keys) > 0 {
		donorKey := keys[0]
		if e := datastore.Get(c, donorKey, this); e != nil {
			return e
		}
		this.ID = donorKey.Encode()
		c.Infof("Returning Donor: %+v", this)
	}
	return nil
}

func (this *Donor) GetByStripeCustomerID(c appengine.Context, stripeCustomerID string) error {
	var donors []Donor
	q := datastore.NewQuery("Donor").Filter("StripeCustomerID =", stripeCustomerID).Ancestor(donorListKey(c))
	keys, err := q.GetAll(c, &donors)
	if err != nil {
	}
	c.Infof("keys: %d", len(keys))
	if len(keys) > 0 {
		donorKey := keys[0]
		if e := datastore.Get(c, donorKey, this); e != nil {
			return e
		}
		this.ID = donorKey.Encode()
		c.Infof("Returning Donor: %+v", this)
	}
	return nil
}

////// Persistence
func (this *Donor) Save(c appengine.Context) error {
	//NEED VALIDATION

	this.Timestamp = time.Now().Unix()
	var myKey *datastore.Key
	myKey, e := datastore.DecodeKey(this.ID)
	if e != nil {
		myKey = datastore.NewKey(c, "Donor", "", 0, donorListKey(c))
	}
	savekey, err := datastore.Put(c, myKey, this)
	if err != nil {
		return err
	}
	this.ID = savekey.Encode()
	return nil
}

func donorListKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "DonorList", "default_donorList", 0, nil)
}

func (this *Donor) UpdateCustomerPaymentInfo(c appengine.Context, stripeCardToken string) error {
	// Stripe: Create or update saved customer
	stripeClient := StripeOPClient(c)
	params := &stripe.CustomerParams{
		Email: this.Email,
		Desc:  this.Email,
	}
	params.SetSource(stripeCardToken)
	//see if donor stripe customer id exists
	if this.StripeCustomerID == "" {
		customer, err := stripeClient.Customers.New(params)
		if err != nil {
			return err
		}
		this.StripeCustomerID = customer.ID
		if e := this.Save(c); e != nil {
			return e
		}
	} else {
		_, e := stripeClient.Customers.Update(this.StripeCustomerID, params)
		if e != nil {
			return e
		}
	}

	return nil
}
